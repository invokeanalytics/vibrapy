'''
Module containing plotting functions for the signal class.
======================================================
Authors: David Hercules Diamond

License: Proprietary
'''

from .plot1d import *