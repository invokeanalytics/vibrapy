''' This module contains functions used for plotting 1 dimensional - (not images) signals.

Authors : DH Diamond
License : MIT
'''


from matplotlib     import pyplot as plt
from numpy.random   import rand

def plot(self, name='', desig='', hold=False, modfunc=None, args={}):
    '''
    Function to plot a signal against it's base.

    Parameters
    ----------

    self : Signal instance.  Is passed automatically if called through
        the signal class.
    name : String. String, list or int for signals to plot.
    
    hold : bool.  True if the plot should not be shown, False 
        otherwise.

    modfunc : Function.  An additional function that can be used to modify the
        signal value while plotting.

    args : Dictionary.  Contains additional keywords for plotting function.
    '''
    if 'marker' in args.keys():
        marker = args['marker']
    else:
        marker = 'o-'
    if modfunc is None:
        modfunc = lambda plot_values: plot_values
    to_plot = self.getSignalNames(name)
    if not desig is '':
        desig = desig + ' - '
    for i in to_plot:
        plt.plot(self.df.index, modfunc(self.df[i]), marker ,\
         alpha=.5, label = desig + i + r' [{}]'.format(self.signalUnit[i])  \
         , color=rand(3))
    plt.tick_params(axis='both', labelsize=30)
    plt.xlabel('{} [${}$]'.format(self.base_type, self.base_unit), fontsize=30)
    if hold == False:
        if not plt.legend() is None:
            plt.legend(prop={'size':30}).draggable(True)
        plt.show()
def semilogy(self, name='', desig='', hold=False, modfunc=None, args={}):
    '''
    Function to plot a signal on a semilogy axis.

    Parameters
    ----------

    self : Signal instance.  Is passed automatically if called through
        the signal class.
    name : String. String, list or int for signals to plot.
    
    hold : bool.  True if the plot should not be shown, False 
        otherwise.

    modfunc : Function.  An additional function that can be used to modify the
        signal value while plotting.

    args : Dictionary.  Contains additional keywords for plotting function.
    '''
    if 'marker' in args.keys():
        marker = args['marker']
    else:
        marker = 'o-'
    if modfunc is None:
        modfunc = lambda plot_values: abs(plot_values)
    to_plot = self.getSignalNames(name)
    if not desig is '':
        desig = desig + ' - '
    for i in to_plot:
        plt.semilogy(self.df.index, modfunc(self.df[i]),marker, alpha=.5,\
         label = desig + i + r' [{}]'.format(self.signalUnit[i]), color=rand(3))
    plt.tick_params(axis='both', labelsize=30)
    plt.xlabel('{} [${}$]'.format(self.base_type, self.base_unit), fontsize=30)
    if hold == False:
        if not plt.legend() is None:
            plt.legend(prop={'size':30}).draggable(True)
        plt.show()

def bar(self, name='', desig='', hold=False, modfunc=None, args={}):
    '''
    Function to plot a signal as a bar plot.

    Parameters
    ----------

    self : Signal instance.  Is passed automatically if called through
        the signal class.
    name : String. String, list or int for signals to plot.
    
    hold : bool.  True if the plot should not be shown, False 
        otherwise.

    modfunc : Function.  An additional function that can be used to modify the
        signal value while plotting.

    args : Dictionary.  Contains additional keywords for plotting function.
    '''
    if modfunc is None:
        modfunc = lambda plot_values: abs(plot_values)
    to_plot = self.getSignalNames(name)
    if not desig is '':
        desig = desig + ' - '
    dBase = self.df.index[2] - self.df.index[1]
    for i in to_plot:
        plt.bar(self.df.index, modfunc(self.df[i]), alpha=.5,\
         label = desig + i + r' [{}]'.format(self.signalUnit[i]),\
          align='center', width=dBase, color=rand(3))
    plt.tick_params(axis='both', labelsize=30)
    plt.xlabel('{} [${}$]'.format(self.base_type, self.base_unit), fontsize=30)
    if hold == False:
        if not plt.legend() is None:
            plt.legend(prop={'size':30}).draggable(True)
        plt.show()