''' A module containing simulation functions and classes
for IAS signal processing.
===============================================
Authors: David Hercules Diamond

License: MIT

'''

from vibrapy import types as __types__
import numpy as __np__
from vibrapy import Signal
from vibrapy.algorithms import general as g


class simulatePolynomialOPR:
    ''' A function that simulates an OPR signal from a 
    polynomial speed function sampled at 
    a certain rate.
    '''
    def __init__(self):
        pass
    def setSpeedFunc(self, speed_poly):
        
        ''' A function used to set this objects
        self.speed_poly function. This function should
        returns 
        
        Parameters
        ----------
        
        speed_poly: A numpy array where the polynomial terms will be evaluated with 
        __np__.polyval. If the speed equation is speed = t*4**3 + t*9**2 + t*10.5**1 + 30 then
        speed_poly = [4,9,10.5,30]. This function must be in units of rad/second.
        
        '''

        self.speed_poly = speed_poly
        self.pos_poly   = __np__.polyint(self.speed_poly)

    def ias(self, t):
        
        ''' A function used to return te shaft IAS from the self.speed_poly
        values. Returns a Vibrapy signal.
        
        Parameters
        ----------
        
        t : The absolute time values which to use for
        interpolation.
        
        '''
        if __types__.isVibrapy(t):
            t_inset = t.values().flatten()
        else:
            t_inset = t
        return Signal(__np__.polyval(self.speed_poly, t_inset),
                      base=t_inset,
                      signalNames='IAS',
                      signalType='IAS',
                      base_type='Temporal',
                      base_unit='s',
                      signalUnit=r'\frac{rad}{sec}')

    def iap(self, t):

        ''' A function used to return te shaft IAP from the self.pos_poly
        values. Returns a Vibrapy signal.
        
        Parameters
        ----------
        
        t : The absolute time values which to use for
        interpolation.
        
        '''
        
        if __types__.isVibrapy(t):
            t_inset = t.values().flatten()
        else:
            t_inset = t
        
        return Signal(__np__.polyval(self.pos_poly, t_inset),
                      base=t_inset,
                      signalNames='IAP',
                      signalType='IAP',
                      base_type='Temporal',
                      base_unit='s',
                      signalUnit=r'rad')
    def opr(self, t, t_start=0 ,fs=1e6, perfect=False):
        
        ''' A function used to calculate the OPR zero-crossing times
        for current shaft speed at for t_start to t at a sampling frequency
        of fs
        '''
        theta_start   = max([1,__np__.ceil(self.iap(t_start)[0,0]/(2*__np__.pi))])
        theta_end     = __np__.floor(self.iap(t)[0,0]/(2*__np__.pi))
        theta_range   = __np__.arange(theta_start, theta_end + 1) * 2 * __np__.pi
        t_opr_perfect = []
        for theta in theta_range:
            error_poly      = -self.pos_poly
            error_poly[-1]  = error_poly[-1]  +  theta
            roots           = __np__.array([i for i in __np__.roots(__np__.polymul(error_poly, error_poly)) if i.real >=0])
            roots_imag      = abs(roots.imag)
            t_opr_perfect.append(1*roots[roots_imag.argmin()].real)
        dt = 1/fs
        if perfect:
            t_opr = t_opr_perfect
        else:
            t_opr = [(i//dt + 1)*dt for i in t_opr_perfect]
        return Signal(t_opr,
                      signalNames='Zero-crossing times',
                      signalType='Zero-crossing times',
                      base_type='Ordinal',
                      base_unit='Revolution',
                      signalUnit=r's')