''' A module containing simulation functions and classes
for BTT signal processing.
===============================================
Authors: David Hercules Diamond

License: MIT

'''
from vibrapy import types as __types__
import numpy as __np__
from vibrapy import Signal
from vibrapy.algorithms import general as g

class unimodalSynchronousConstantSim:
    '''
    A simulator class that simulates a signal of synchronous vibration
    at a constant shaft speed.
    
    Parameters
    ----------
    
    EO : int. The engine order of Vibration.
    
    sensors: list. A list of sensor positions in units of radians.
    
    A : numeric. The sine wave coefficient
    
    B : numeric. The cosine wave coefficient
    
    C : numeric. The signal offset
    
    revs: The number of revolutions to simulate
    
    sigma : The standard deviation of normally distributed noise
        to add.
    '''
    def __init__(self, EO, sensors, A=167, B=167, C=0, revs=20, sigma=20):
        if not __types__.isIntnotNull(EO):
            __types__.isIntnotNull(EO, 'EO')
        self.EO = EO*1
        if not __types__.isIter(sensors):
            __types__.isIntnotNull(sensors, 'sensors')
        self.sensors = __np__.array(sensors)
        self.A = A*1
        self.B = B*1
        self.C = C*1
        self.revs = revs*1
        if sigma >= 0:
            self.sigma = sigma*1
        else:
            ValueError('Sigma must be positive or zero')
    def sim(self):
        '''
        A function that simulates a BTT signal according to the simulator's
        characteristics.

        Returns
        -------

        X : Vibrapy signal of the tip deflections at the sensors.

        AoA : Vibrapy signal of the angles of arrival at the sensors.
        '''
        angles = []
        for i in range(self.revs):
            angles.append(list(self.sensors + __np__.pi * 2 * i))
        angles = __np__.array(angles)
        x = self.A * __np__.sin(self.EO * angles) + self.B * __np__.cos(self.EO * angles) + self.C * __np__.ones_like(angles)
        if self.sigma:
            x += __np__.random.rand(angles.shape[0], angles.shape[1]) * self.sigma
        return Signal(x,signalNames='x_1',signalType='Displacement',signalUnit='mu m',base_type='Temporal',base_unit='Revolution'),\
        Signal(angles, signalNames='theta_1',signalType='Location',signalUnit='radians',base_type='Temporal',base_unit='Revolution')