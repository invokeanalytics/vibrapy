'''
Module containing the base modules for vibrapy.
======================================================
Authors: David Hercules Diamond

License: MIT
'''

from .Signal import *