'''
Module containing the base signal classes for Vibrapy.
======================================================
Authors: David Hercules Diamond

License: MIT
'''
import warnings

from pandas import DataFrame, Series
from numpy import array, ndarray, arange, zeros, floor, pi,\
                  ediff1d, cumsum, atleast_2d, ones
import importlib

import inspect
from   vibrapy.utils    import types
import vibrapy.plot   as vibrapy_plot
import copy
import os
from time import time, sleep

import sys
import traceback

try:
    '''
    Import seaborn if library is available
    '''
    import seaborn as sns
    sns.set_style('whitegrid')
except:
    pass

def getLabelsFromArg(labels, shape):
    def nextLabel(_labels):
        last_char  = _labels[-1][-1]
        word_rest  = _labels[-1][:-1]
        if last_char in types.BASE58:
            int_base58 =  types.BASE58.index(last_char)
            return [word_rest + types.BASE58[(int_base58+1) % 58] ]
        else:
            return [word_rest + chr(ord(last_char)+1)]

    if isinstance(labels, list) and not types.isEmptyList(labels):
        labels = [str(i).strip() for i in labels]
    elif isinstance(labels, str):
        labels = [labels.strip()]
    else:
        labels = ['x']
    while len(labels) < shape:
        labels = labels + nextLabel(labels)
    return labels[:shape]

class oneDSignalBase(object):
    def __init__(self):
        self.verbose = False
        self.temp_time = time()
    def trackTime(self, message=''):
        if self.verbose:
            duration = time() - self.temp_time
            print('Line ', inspect.currentframe().f_back.f_lineno, message, 'Time taken : {0:.3f} seconds'.format(duration))
            self.temp_time = time()
            sleep(0.1)
    def __call__(self, *args):
        '''
        Function that gets called when the angularSignal is called.
        Executes a number of operations on the original signal based on a
        series of piped instructions.  Returns a signal with all the
        modifications applied.  Note: this returns a Signal instance, 
        not an angularSignal.

        Parameters
        ----------
        text : Piped instructions

        Returns
        -------

        A signal with the processed results.
        '''
        
        signals_to_take  = list(self.df.columns)
        temp_matrix      = copy.deepcopy(self.df.as_matrix())
        temp_base        = copy.deepcopy(self.df.index.values)
        temp_signal_type = copy.deepcopy([self.signalType[i] for i in signals_to_take])
        temp_signal_unit = copy.deepcopy([self.signalUnit[i] for i in signals_to_take])
        temp_base_type   = copy.deepcopy(self.base_type)
        temp_base_unit   = copy.deepcopy(self.base_unit)

        no = 0
        for j in args:
            try:
                temp_matrix,     \
                temp_base,       \
                signals_to_take, \
                temp_signal_type,\
                temp_signal_unit,\
                temp_base_type,  \
                temp_base_unit = j.process(temp_matrix,temp_base,signals_to_take,
                        temp_signal_type,temp_signal_unit,temp_base_type,temp_base_unit, self)
            except Exception as e:
                print('The processing algorithm {} is not a valid algorithm. Skipping this algorithm.'.format(j))
                print('The following error message was returned:\n\n')
                top = traceback.extract_tb(sys.exc_info()[2])[-1]
                print('{} : {} in {} at line {}'.format(type(e).__name__, str(e), os.path.basename(top[0]), str(top[1])))
        return buildSignalFromContituents(temp_matrix,\
                            temp_base,       \
                            signals_to_take, \
                            temp_signal_type,\
                            temp_signal_unit,\
                            temp_base_type,  \
                            temp_base_unit)
    def __repr__(self):
        '''
        signal's print function
        '''
        type_str = ''
        print_mat     = []
        print_index   = []
        print_columns = ['Signal type', 'Signal unit']
        for i in self.df.columns.tolist():
            print_mat.append([self.signalType[i], self.signalUnit[i]])
            print_index.append(i)
        return DataFrame(print_mat, index=print_index, columns=print_columns).__repr__()+ '\n\n' + self.df.head(10).__repr__() + '\n\n' + \
        'Base type:\t' + self.base_type+'\n'+ \
        'Base unit:\t' + self.base_unit + '\n'
    def _repr_html_(self):
        '''
        signal's print function for html output
        '''
        type_str = ''
        print_mat     = []
        print_index   = []
        print_columns = ['Signal type', 'Signal unit']
        for i in self.df.columns.tolist():
            print_mat.append([self.signalType[i], self.signalUnit[i]])
            print_index.append(i)
        return '<h1>Signal metadata<h1>\n\n' +\
         DataFrame(print_mat, index=print_index, columns=print_columns)._repr_html_() +\
         '<h1>Values<h1>\n\n' +\
         '\n\n' + self.df.head(10)._repr_html_() + '\n\n' + \
         '<h1>Base metadata<h1>' +\
        DataFrame([self.base_type, self.base_unit], index=['Base type:','Base unit:' ], columns=[''])._repr_html_()
    def __getitem__(self, key, for_setitem=False):
        def selectSignals(select_signal_names, select_base_boolean):
            '''
            Function to select a value from a list of signal names and 
            a  boolean array of base values
            '''
            if sum(select_base_boolean) == 0:
                raise ValueError('No valid base values selected')

            if for_setitem == True:
                return select_signal_names, select_base_boolean

            if len(select_signal_names) == 0:
                raise ValueError('No valid signals selected')
            temp_matrix      =\
             copy.deepcopy(self.df.loc[select_base_boolean,\
             select_signal_names].as_matrix())
            if len(temp_matrix.flatten()) == 1:
                return temp_matrix.flatten()[0]
            temp_base        =\
             copy.deepcopy(array(self.df[select_base_boolean]\
                .index.tolist()))
            signals_to_take  = copy.deepcopy(select_signal_names)
            temp_signal_type =\
             copy.deepcopy([self.signalType[i]\
              for i in select_signal_names])
            temp_signal_unit =\
             copy.deepcopy([self.signalUnit[i]\
              for i in select_signal_names])
            temp_base_type   =\
             copy.deepcopy(self.base_type)
            temp_base_unit   =\
             copy.deepcopy(self.base_unit)
            return buildSignalFromContituents(temp_matrix,\
                    temp_base,       \
                    signals_to_take, \
                    temp_signal_type,\
                    temp_signal_unit,\
                    temp_base_type,  \
                    temp_base_unit)
        def getSignalsFromSlice(key):
            possible_signal_names = self.df.columns.tolist()
            number_of_signals     = len(possible_signal_names)
            # Get step amount
            if key.step is None:
                step = 1
            elif not types.isIntnotNull(key.step):
                types.isIntnotNull(key.step, 'step')
            else:
                step  = key.step
            # Get start integer
            if key.start is None:
                start = 0
            elif types.isStr(key.start):
                if key.start not in possible_signal_names:
                    raise ValueError('Start signal of {} does not exist'.\
                        format(key.start))
                else:
                    start =  possible_signal_names.index(key.start)
            elif types.isInt(key.start):
                if key.start < 0:
                    if -1*key.start > number_of_signals:
                        raise ValueError('Start is out of range')
                    else:
                        start = key.start
                else:
                    if key.start < number_of_signals:
                        start = key.start
                    else:
                        raise ValueError('Start is out of range')
            # Get stop integer
            if key.stop is None:
                stop = number_of_signals
            elif types.isStr(key.stop):
                if key.stop not in possible_signal_names:
                    raise ValueError('Stop signal of {} does not exist'.\
                        format(key.stop))
                else:

                    stop_between = possible_signal_names.index(key.stop)
                    if stop_between < start:
                        if stop_between != 0:
                            stop =  stop_between - 1
                        else:
                            stop = None 
                    else:
                        stop = stop_between + 1
            elif types.isInt(key.stop):
                if key.stop < 0:
                    if -1*key.stop > number_of_signals:
                        raise ValueError('Stop is out of range')
                    else:
                        stop = number_of_signals + key.stop + 1
                else:
                    stop = key.stop + 1
            if stop == None:
                if step == 1:
                    step = -1
            elif start > stop and step == 1:
                step = -1
            return possible_signal_names[start:stop:step]
        def getSignalsFromStr(key):
            names = self.getSignalNames(key, reverse_role=True)
            if names == [] and for_setitem==True:
                return [key]
            else:
                return names
        def getSignalsFromInt(key):
            possible_signal_names = self.df.columns.tolist()
            number_of_signals     = len(possible_signal_names)
            temp_signal_names = []
            if key >= 0 and key < number_of_signals:
                temp_signal_names.append(possible_signal_names[key])
            return temp_signal_names
        def getSignalsFromList(key):
            if types.isEmptyList(key):
                raise ValueError('Slicing key cannot be an empty list.')
            if types.isListofStr(key):
                select_signal_string = ','.join(key)
                # Select signals according to syntax
                temp_signal_names = self.getSignalNames(select_signal_string, reverse_role=True)
                if for_setitem == True:
                    if temp_signal_names == []:
                        return key
                    elif len(temp_signal_names) != len(key):
                        raise ValueError('You cannot set existing and new signals names simultaneously')
                # Return signal
                return temp_signal_names
            elif types.isListofInt(key):
                possible_signal_names = self.df.columns.tolist()
                number_of_signals     = len(possible_signal_names)
                temp_signal_names = []
                for i in key:
                    if i >= 0 and i < number_of_signals:
                        temp_signal_names.append(possible_signal_names[i])
                
                return temp_signal_names
            else:
                raise ValueError('The key must be a list of pure strings '+\
                    'or integers')
        self.trackTime('__getitem__------------------------------{')
        if types.isInt(key):
            temp_signal_names  = getSignalsFromInt(key)
            base_bool       = ones(len(self.df), dtype=bool)
            return selectSignals(temp_signal_names, base_bool)
        self.trackTime()
        if types.isStr(key):
            # Select signals according to syntax
            temp_signal_names = getSignalsFromStr(key)
            # Select all base
            base_bool       = ones(len(self.df), dtype=bool)
            # Return signal
            return selectSignals(temp_signal_names, base_bool)
        elif types.isList(key):
            temp_signal_names = getSignalsFromList(key)
            # Select all base
            base_bool         = ones(len(self.df), dtype=bool)
            # Return signal
            return selectSignals(temp_signal_names, base_bool)
        elif isinstance(key, slice):
            temp_signal_names   = getSignalsFromSlice(key)
            base_bool           = ones(len(self.df), dtype=bool)
            return selectSignals(temp_signal_names, base_bool)
        elif isinstance(key, tuple): # Here the user selects both the
            self.trackTime()
            if len(key)    == 2:     # base and the signals.
                key_signal = key[1]
                self.trackTime()
                # Determine the signals to take
                if types.isInt(key_signal):
                    temp_signal_names  = getSignalsFromInt(key_signal)
                elif types.isStr(key_signal):
                    # Select signals according to syntax
                    temp_signal_names = getSignalsFromStr(key_signal)
                elif types.isList(key_signal):
                    temp_signal_names = getSignalsFromList(key_signal)
                elif isinstance(key_signal, slice):
                    temp_signal_names   = getSignalsFromSlice(key_signal)
                else:
                    raise ValueError('Signal indexer {} not supported'.format(key_signal))
                self.trackTime()
                key_base     = key[0]
                if types.isPandSeries(key_base):
                    key_base = key_base.as_matrix().flatten()
                self.trackTime()
                index_arange = arange(len(self.df.index))
                # Check if key is slice
                self.trackTime()
                if types.isSlice(key_base):
                    # Get index beginning
                    self.trackTime()
                    if key_base.start is None:
                        start = 0
                    elif types.isInt(key_base.start):
                        start = key_base.start
                    elif types.isNum(key_base.start):
                        start_arr = index_arange\
                        [self.df.index >= key_base.start]
                        if len(start_arr) == 0:
                            raise ValueError('Base start index '+\
                                'out of bounds')
                        else:
                            start = start_arr[0]
                    else:
                        raise ValueError('Could not parse start index')
                    # Get index stop
                    self.trackTime()
                    if key_base.stop is None:
                        stop = len(self.df.index)
                    elif types.isInt(key_base.stop):
                        if key_base.stop < 0: 
                            stop = len(self.df.index) + key_base.stop + 1
                        else:
                            stop = key_base.stop + 1 
                    elif types.isNum(key_base.stop):
                        stop_arr = index_arange\
                        [self.df.index   <= key_base.stop]
                        if len(stop_arr) == 0:
                            raise ValueError('Base stop index '+\
                                'out of bounds')
                        else:
                            stop = stop_arr[-1]
                    else:
                        raise ValueError('Could not parse stop index')
                    self.trackTime()
                    if stop <= start:
                        raise ValueError('No base values for this range')
                    # Get index step
                    self.trackTime()
                    if key_base.step is None:
                        base_bool = zeros(len(self.df.index), dtype=bool)
                        base_bool[start:stop] = True
                    self.trackTime()
                    if types.isInt(key_base.step):
                        base_bool = zeros(len(self.df.index), dtype=bool)
                        base_bool[start:stop:key_base.step] = True
                    elif types.isNum(key_base.step):
                        if key_base.step <=0:
                            raise ValueError('Step is smaller or equal to zero')
                        # Indexer takes a value every "step" value of base.
                        threshhold_values = arange(self.df.index[start], self.df.index[stop], key_base.step)
                        base_bool = zeros(len(self.df.index), dtype=bool)
                        for i in threshhold_values:
                            step_arr   = index_arange[self.df.index >= i]
                            self.trackTime()
                            if len(step_arr) > 0:
                                base_bool[step_arr[0]] = True
                elif types.isInt(key_base):
                    base_bool = zeros(len(self.df.index), dtype=bool)
                    self.trackTime()
                    if key_base in index_arange:
                        base_bool[key_base] = True
                elif types.isNDorListofInt(key_base):
                    key_base          = array(key_base)
                    key_base_filtered = key_base\
                    [(key_base >=-len(index_arange)) &\
                     ( key_base < len(index_arange))]

                    base_bool = zeros(len(self.df.index), dtype=bool)
                    base_bool[key_base_filtered] = True
                    self.trackTime()
                elif types.isNDorListofBool(key_base):
                    if len(key_base) == len(index_arange):
                        base_bool    = array(key_base)
                    else:
                        raise ValueError('Boolean array is not'+\
                            ' the same length as base')
                    self.trackTime()
                elif types.isNum(key_base):
                    # This is useful selection is performed when the index
                    # selector is a float of a number. The number must be
                    # within 1e-8 of a value in df.index. Otherwise return
                    # an error
                    self.trackTime()
                    base_bool = zeros(len(self.df.index), dtype=bool)
                    tol = 1e-8
                    index_tol = abs(array(self.df.index) - key_base)
                    self.trackTime()
                    if any(index_tol < tol):
                        base_bool[index_tol < tol] = True
                    self.trackTime()
                else:
                    raise ValueError('No support for base indexer '+\
                        '{}.'.format(key_base))
                self.trackTime()
            else:
                raise ValueError('Indexer {} is not of the correct shape '.\
                        format(key))
            self.trackTime()
            return selectSignals(temp_signal_names, base_bool)
        else:
            raise ValueError('Slicing key {} not supported.'.\
                format(type(key)))
        self.trackTime('}-----------------------------__getitem__')
        return self
    def __setitem__(self, key, set_value):
        ''' Sets an item in the vibrapy object. Limited functionality at the moment.
        '''
        def getArraySet(base_select):
            ''' A function to return the array that will be used for setting.
            '''
            # metadata only if the system is a Vibrapy signal.
            # will contain signalType and signalUnit info
            metadata = {}
            # if set_value is a vibrapy signal
            if types.isStr(set_value):
                raise ValueError('Vibrapy does not allow string values.')
            if types.isVibrapy(set_value):
                # Check that signal is 1D.
                # Expand to multiple cases further along
                if set_value.shape[1] == 1:
                    _, _, temp_signal_type, temp_signal_unit, _, _ = set_value._getSignalMetadata()
                    metadata['signal_type'] = temp_signal_type[0]
                    metadata['signal_unit'] = temp_signal_unit[0]
                    return set_value.values().flatten(), metadata
                else:
                    raise ValueError('Assigning 2D Signals or arrays not currently supported.')
            elif types.isPandSeries(set_value):
                # Pandas series is returned as an array
                return array(set_value), metadata
            elif types.isIter(set_value):
                # Iterdepth must be 1
                if types.getIterDepth(set_value) > 1:
                    raise ValueError('The depth of the setting iterable must be 1.')
                # Return as numpy array
                return array(set_value), metadata
            elif types.isNum(set_value):
                # Make an empty array with the same shape as
                # base_select
                return ones(base_select.sum()) * set_value, metadata

        names, base_select = self.__getitem__(key, for_setitem=True)
        if names == []:
            raise ValueError('Cannot set to an unspecified name')
        array_to_set, signal_metadata = getArraySet(base_select)
        if len(array_to_set) != base_select.sum():
            raise ValueError('Shape mismatch. Selected base is {} values and setting_array is {} values.'.format(base_select.sum(), len(array_to_set)))
        signal_names, temp_base, temp_signal_type, temp_signal_unit, temp_base_type, temp_base_unit = self._getSignalMetadata()
        # start assigning
        if list(set(names) & set(signal_names)) != []:
            # names is a subset of signal_names
            # recall that all names in names must be
            # in signal_names otherwise the software
            # crashes in __getitem__.getSignalsFromList
            for i in names:
                self.df.loc[base_select, i] = array_to_set
                if 'signal_type' in signal_metadata.keys():
                    # Can only change the type and unit if
                    # the selected base is all values.
                    if base_select.sum() == self.shape[0]:
                        self.signalType[i] = signal_metadata['signal_type']
                        self.signalUnit[i] = signal_metadata['signal_unit']
        else:
            # A new value is going to be set. Assign a 
            # new value to the current df and add the relevant
            
            # for new variables, you cannot set values other than the entire
            # array.
            if base_select.sum() != self.shape[0]:
                raise ValueError('Cannot set a subset of a new signal. Ensure the setting array length and the base length of the target signal is equal.')
            # Create each new array and set the values
            for i in names:
                self.df[i] = array_to_set
                if 'signal_type' in signal_metadata.keys():
                    # Set the type and unit
                    self.signalType[i] = signal_metadata['signal_type']
                    self.signalUnit[i] = signal_metadata['signal_unit']
                else:
                    # Set the type and unit to .
                    self.signalType[i] = '.'
                    self.signalUnit[i] = '.'
        self.shape      = (len(self.df.index), \
                           len(self.df.columns.tolist()))
    def _ipython_key_completions_(self):
        return self.df.columns.tolist()
    def __eq__(self, other):
        if types.isNum(other):
            # Float or int
            base_bool = ones(len(self.df.index), dtype=bool)
            for i in self.df.columns.tolist():
                base_bool = base_bool & (self.df[i] == other)
            return base_bool
        elif types.isPand(other):
            other_comp = other.as_matrix()
            if len(other_comp.shape) == 1:
                other_comp = atleast_2d(other_comp).T
        elif types.isList(other):
            other_comp = atleast_2d(other)
            if types.getIterDepth(other) == 1:
                other_comp = atleast_2d(other).T
        elif types.isVibrapy(other):
            other_comp = other.values()
        elif types.isND(other):
            other_comp = atleast_2d(other)
            if len(other.shape) == 1:
                other_comp = atleast_2d(other_comp).T
        else:
            raise ValueError('Cannot compare singal to {}'.\
                format(type(other)))
        if types.getIterDepth(other_comp) != 2:
            raise ValueError('Cannot compare signal to object with a '+\
                'depth larger than 2')
        if other_comp.shape[0] != len(self.df.index):
            raise ValueError('Cannot compare signal with base length '+\
                '{} with {} with length {}.'.\
                format(len(self.df.index),type(other),\
                other_comp.shape[0]))
        if len(self.df.columns.tolist()) != 1:
            if other_comp.shape[1] != len(self.df.columns.tolist()):
                raise ValueError('{} represents {} signals.'.\
                    format(type(other),other_comp.shape[1])+\
                    ' Can only compare with 1 or {} signals'.\
                    format(len(self.df.columns.tolist())))

        if len(self.df.columns.tolist()) == 1:
            base_bool = ones(len(self.df.index), dtype=bool)
            for i in range(other_comp.shape[1]):
                base_bool = base_bool & (self.df.iloc[:, 0] == other_comp[:,i])
        else:
            base_bool = ones(len(self.df.index), dtype=bool)
            for i, j in enumerate(self.df.columns.tolist()):
                base_bool = base_bool &\
                (self.df.iloc[:, i] == other_comp[:,i])
        return base_bool
    def __ne__(self, other):
        if types.isNum(other):
            # Float or int
            base_bool = ones(len(self.df.index), dtype=bool)
            for i in self.df.columns.tolist():
                base_bool = base_bool & (self.df[i] != other)
            return base_bool
        elif types.isPand(other):
            other_comp = other.as_matrix()
            if len(other_comp.shape) == 1:
                other_comp = atleast_2d(other_comp).T
        elif types.isList(other):
            other_comp = atleast_2d(other)
            if types.getIterDepth(other) == 1:
                other_comp = atleast_2d(other).T
        elif types.isVibrapy(other):
            other_comp = other.values()
        elif types.isND(other):
            other_comp = atleast_2d(other)
            if len(other.shape) == 1:
                other_comp = atleast_2d(other_comp).T
        else:
            raise ValueError('Cannot compare singal to {}'.\
                format(type(other)))
        if types.getIterDepth(other_comp) != 2:
            raise ValueError('Cannot compare signal to object with a '+\
                'depth larger than 2')
        if other_comp.shape[0] != len(self.df.index):
            raise ValueError('Cannot compare signal with base length '+\
                '{} with {} with length {}.'.\
                format(len(self.df.index),type(other),\
                other_comp.shape[0]))
        if len(self.df.columns.tolist()) != 1:
            if other_comp.shape[1] != len(self.df.columns.tolist()):
                raise ValueError('{} represents {} signals.'.\
                    format(type(other),other_comp.shape[1])+\
                    ' Can only compare with 1 or {} signals'.\
                    format(len(self.df.columns.tolist())))

        if len(self.df.columns.tolist()) == 1:
            base_bool = ones(len(self.df.index), dtype=bool)
            for i in range(other_comp.shape[1]):
                base_bool = base_bool & (self.df.iloc[:, 0] != other_comp[:,i])
        else:
            base_bool = ones(len(self.df.index), dtype=bool)
            for i, j in enumerate(self.df.columns.tolist()):
                base_bool = base_bool &\
                (self.df.iloc[:, i] != other_comp[:,i])
        return base_bool
    def __lt__(self, other):
        if types.isNum(other):
            # Float or int
            base_bool = ones(len(self.df.index), dtype=bool)
            for i in self.df.columns.tolist():
                base_bool = base_bool & (self.df[i] < other)
            return base_bool
        elif types.isPand(other):
            other_comp = other.as_matrix()
            if len(other_comp.shape) == 1:
                other_comp = atleast_2d(other_comp).T
        elif types.isList(other):
            other_comp = atleast_2d(other)
            if types.getIterDepth(other) == 1:
                other_comp = atleast_2d(other).T
        elif types.isVibrapy(other):
            other_comp = other.values()
        elif types.isND(other):
            other_comp = atleast_2d(other)
            if len(other.shape) == 1:
                other_comp = atleast_2d(other_comp).T
        else:
            raise ValueError('Cannot compare singal to {}'.\
                format(type(other)))
        if types.getIterDepth(other_comp) != 2:
            raise ValueError('Cannot compare signal to object with a '+\
                'depth larger than 2')
        if other_comp.shape[0] != len(self.df.index):
            raise ValueError('Cannot compare signal with base length '+\
                '{} with {} with length {}.'.\
                format(len(self.df.index),type(other),\
                other_comp.shape[0]))
        if len(self.df.columns.tolist()) != 1:
            if other_comp.shape[1] != len(self.df.columns.tolist()):
                raise ValueError('{} represents {} signals.'.\
                    format(type(other),other_comp.shape[1])+\
                    ' Can only compare with 1 or {} signals'.\
                    format(len(self.df.columns.tolist())))

        if len(self.df.columns.tolist()) == 1:
            base_bool = ones(len(self.df.index), dtype=bool)
            for i in range(other_comp.shape[1]):
                base_bool = base_bool & (self.df.iloc[:, 0] < other_comp[:,i])
        else:
            base_bool = ones(len(self.df.index), dtype=bool)
            for i, j in enumerate(self.df.columns.tolist()):
                base_bool = base_bool &\
                (self.df.iloc[:, i] < other_comp[:,i])
        return base_bool
    def __le__(self, other):
        if types.isNum(other):
            # Float or int
            base_bool = ones(len(self.df.index), dtype=bool)
            for i in self.df.columns.tolist():
                base_bool = base_bool & (self.df[i] <= other)
            return base_bool
        elif types.isPand(other):
            other_comp = other.as_matrix()
            if len(other_comp.shape) == 1:
                other_comp = atleast_2d(other_comp).T
        elif types.isList(other):
            other_comp = atleast_2d(other)
            if types.getIterDepth(other) == 1:
                other_comp = atleast_2d(other).T
        elif types.isVibrapy(other):
            other_comp = other.values()
        elif types.isND(other):
            other_comp = atleast_2d(other)
            if len(other.shape) == 1:
                other_comp = atleast_2d(other_comp).T
        else:
            raise ValueError('Cannot compare singal to {}'.\
                format(type(other)))
        if types.getIterDepth(other_comp) != 2:
            raise ValueError('Cannot compare signal to object with a '+\
                'depth larger than 2')
        if other_comp.shape[0] != len(self.df.index):
            raise ValueError('Cannot compare signal with base length '+\
                '{} with {} with length {}.'.\
                format(len(self.df.index),type(other),\
                other_comp.shape[0]))
        if len(self.df.columns.tolist()) != 1:
            if other_comp.shape[1] != len(self.df.columns.tolist()):
                raise ValueError('{} represents {} signals.'.\
                    format(type(other),other_comp.shape[1])+\
                    ' Can only compare with 1 or {} signals'.\
                    format(len(self.df.columns.tolist())))
        if len(self.df.columns.tolist()) == 1:
            base_bool = ones(len(self.df.index), dtype=bool)
            for i in range(other_comp.shape[1]):
                base_bool = base_bool & (self.df.iloc[:, 0] <= other_comp[:,i])
        else:
            base_bool = ones(len(self.df.index), dtype=bool)
            for i, j in enumerate(self.df.columns.tolist()):
                base_bool = base_bool &\
                (self.df.iloc[:, i] <= other_comp[:,i])
        return base_bool
    def __gt__(self, other):
        if types.isNum(other):
            # Float or int
            base_bool = ones(len(self.df.index), dtype=bool)
            for i in self.df.columns.tolist():
                base_bool = base_bool & (self.df[i] > other)
            return base_bool
        elif types.isPand(other):
            other_comp = other.as_matrix()
            if len(other_comp.shape) == 1:
                other_comp = atleast_2d(other_comp).T
        elif types.isList(other):
            other_comp = atleast_2d(other)
            if types.getIterDepth(other) == 1:
                other_comp = atleast_2d(other).T
        elif types.isVibrapy(other):
            other_comp = other.values()
        elif types.isND(other):
            other_comp = atleast_2d(other)
            if len(other.shape) == 1:
                other_comp = atleast_2d(other_comp).T
        else:
            raise ValueError('Cannot compare singal to {}'.\
                format(type(other)))
        if types.getIterDepth(other_comp) != 2:
            raise ValueError('Cannot compare signal to object with a '+\
                'depth larger than 2')
        if other_comp.shape[0] != len(self.df.index):
            raise ValueError('Cannot compare signal with base length '+\
                '{} with {} with length {}.'.\
                format(len(self.df.index),type(other),\
                other_comp.shape[0]))
        if len(self.df.columns.tolist()) != 1:
            if other_comp.shape[1] != len(self.df.columns.tolist()):
                raise ValueError('{} represents {} signals.'.\
                    format(type(other),other_comp.shape[1])+\
                    ' Can only compare with 1 or {} signals'.\
                    format(len(self.df.columns.tolist())))
        if len(self.df.columns.tolist()) == 1:
            base_bool = ones(len(self.df.index), dtype=bool)
            for i in range(other_comp.shape[1]):
                base_bool = base_bool & (self.df.iloc[:, 0] > other_comp[:,i])
        else:
            base_bool = ones(len(self.df.index), dtype=bool)
            for i, j in enumerate(self.df.columns.tolist()):
                base_bool = base_bool &\
                (self.df.iloc[:, i] > other_comp[:,i])
        return base_bool
    def __ge__(self, other):
        
        ''' Implements the greater than or equal operator.
        '''
        if types.isNum(other):
            # Float or int
            base_bool = ones(len(self.df.index), dtype=bool)
            for i in self.df.columns.tolist():
                base_bool = base_bool & (self.df[i] >= other)
            return base_bool
        elif types.isPand(other):
            other_comp = other.as_matrix()
            if len(other_comp.shape) == 1:
                other_comp = atleast_2d(other_comp).T
        elif types.isList(other):
            other_comp = atleast_2d(other)
            if types.getIterDepth(other) == 1:
                other_comp = atleast_2d(other).T
        elif types.isVibrapy(other):
            other_comp = other.values()
        elif types.isND(other):
            other_comp = atleast_2d(other)
            if len(other.shape) == 1:
                other_comp = atleast_2d(other_comp).T
        else:
            raise ValueError('Cannot compare singal to {}'.\
                format(type(other)))
        if types.getIterDepth(other_comp) != 2:
            raise ValueError('Cannot compare signal to object with a '+\
                'depth larger than 2')
        if other_comp.shape[0] != len(self.df.index):
            raise ValueError('Cannot compare signal with base length '+\
                '{} with {} with length {}.'.\
                format(len(self.df.index),type(other),\
                other_comp.shape[0]))
        if len(self.df.columns.tolist()) != 1:
            if other_comp.shape[1] != len(self.df.columns.tolist()):
                raise ValueError('{} represents {} signals.'.\
                    format(type(other),other_comp.shape[1])+\
                    ' Can only compare with 1 or {} signals'.\
                    format(len(self.df.columns.tolist())))
        if len(self.df.columns.tolist()) == 1:
            base_bool = ones(len(self.df.index), dtype=bool)
            for i in range(other_comp.shape[1]):
                base_bool = base_bool & (self.df.iloc[:, 0] <= other_comp[:,i])
        else:
            base_bool = ones(len(self.df.index), dtype=bool)
            for i, j in enumerate(self.df.columns.tolist()):
                base_bool = base_bool &\
                (self.df.iloc[:, i] >= other_comp[:,i])
        return base_bool
    def __add__(self, other):
        return self._elementWiseOp(other, lambda arg1, arg2: arg1 + arg2, lambda arg1, arg2: '({} + {})'.format(arg1, arg2))
    def __sub__(self, other):
        return self._elementWiseOp(other, lambda arg1, arg2: arg1 - arg2, lambda arg1, arg2: '({} - {})'.format(arg1, arg2))
    def __mul__(self, other):
        return self._elementWiseOp(other, lambda arg1, arg2: arg1 * arg2, lambda arg1, arg2: '({} * {})'.format(arg1, arg2))
    def __pow__(self, other):
        return self._elementWiseOp(other, lambda arg1, arg2: arg1**arg2,  lambda arg1, arg2: '('+arg1 + r'^{(' + arg2 + r')})')
    def __truediv__(self, other):
        return self._elementWiseOp(other, lambda arg1, arg2: arg1 / arg2, lambda arg1, arg2: '({} / {})'.format(arg1, arg2))
    
    def __radd__(self, other):
        return self._elementWiseOp(other, lambda arg1, arg2: arg1 + arg2, lambda arg1, arg2: '({} + {})'.format(arg2, arg1))
    
    def __rsub__(self, other):
        return self._elementWiseOp(other, lambda arg1, arg2: arg2 - arg1, lambda arg1, arg2: '({} - {})'.format(arg2, arg1))
    
    def __rmul__(self, other):
        return self._elementWiseOp(other, lambda arg1, arg2: arg2 * arg1, lambda arg1, arg2: '({} * {})'.format(arg2, arg1))
    
    def __rpow__(self, other):
        return self._elementWiseOp(other, lambda arg1, arg2: arg2**arg1,  lambda arg1, arg2:  '('+arg2 + r'^{(' + arg1 + r')})')
    
    def __rtruediv__(self, other):
        return self._elementWiseOp(other, lambda arg1, arg2: arg2 / arg1, lambda arg1, arg2: '({} / {})'.format(arg2, arg1))
    
    def _elementWiseOp(self, other, func, unit_func):
        
        ''' This function implements various element-wise arithmetic operations.
        This method also changes the unit of the signal accordingly.
        '''
        temp_matrix = copy.deepcopy(self.df.as_matrix())

        _, _, _, temp_signal_unit, _, _ = self._getSignalMetadata()
        no_unit = '.'
        othercomp_units = []
        if types.isNum(other):
            # Float or int
            for i in range(temp_matrix.shape[1]):
                temp_matrix[:, i] = func(temp_matrix[:, i], other)
                temp_signal_unit[i] = unit_func( temp_signal_unit[i], no_unit)
            return buildSignalFromContituents(temp_matrix,\
                    self.df.index.tolist(),       \
                    self.df.columns.tolist(), \
                    self.signalType,\
                    temp_signal_unit,\
                    self.base_type,  \
                    self.base_unit)
        elif types.isPand(other):
            other_comp = other.as_matrix()
            if len(other_comp.shape) == 1:
                other_comp = atleast_2d(other_comp).T
            othercomp_units = [no_unit for i in range(other_comp.shape[1])]
        elif types.isList(other):
            other_comp = atleast_2d(other)
            if types.getIterDepth(other) == 1:
                other_comp = atleast_2d(other).T
            othercomp_units = [no_unit for i in range(other_comp.shape[1])]
        elif types.isVibrapy(other):
            other_comp = other.values()
            _, _, _, othercomp_units, _, _ = other._getSignalMetadata()
        elif types.isND(other):
            other_comp = atleast_2d(other)
            if len(other.shape) == 1:
                other_comp = atleast_2d(other_comp).T
            othercomp_units = [no_unit for i in range(other_comp.shape[1])]
        else:
            raise ValueError('Cannot perform arithmetic '+\
                'operation on {}'.format(type(other)))
        if types.getIterDepth(other_comp) != 2:
            raise ValueError('Cannot perform arithmetic operation on object with a '+\
                'depth larger than 2')
        if other_comp.shape[0] != len(self.df.index):
            raise ValueError('Cannot perform arithmetic operation between'\
                +' signal with length {} and object {} with length {}.'.\
                format(len(self.df.index),type(other),\
                other_comp.shape[0]))
        if len(self.df.columns.tolist()) != 1:
            if not other_comp.shape[1] in [1, len(self.df.columns.tolist())]:
                raise ValueError('{} represents {} signals.'.\
                    format(type(other),other_comp.shape[1])+\
                    ' Can only act on 1 or {} signals'.\
                    format(len(self.df.columns.tolist())))
        
        if len(self.df.columns.tolist()) == 1:
            for i in range(other_comp.shape[1]):
                # This statement does not make sense
                # must be cleaned up.
                temp_matrix[:, 0]   = func(temp_matrix[:, 0],other_comp[:,i])
                temp_signal_unit[0] = unit_func(temp_signal_unit[0], othercomp_units[i])
        else:
            if other_comp.shape[1] == 1:
                for i, j in enumerate(self.df.columns.tolist()):
                    temp_matrix[:, i] = func(temp_matrix[:, i],\
                                             other_comp[:,0])
                    temp_signal_unit[i] = unit_func(temp_signal_unit[i], othercomp_units[0])
            else:
                for i, j in enumerate(self.df.columns.tolist()):
                    temp_matrix[:, i] = func(temp_matrix[:, i],\
                                             other_comp[:,i])
                    temp_signal_unit[i] = unit_func(temp_signal_unit[i], othercomp_units[i])
        return buildSignalFromContituents(temp_matrix,\
                    self.df.index.tolist(),       \
                    self.df.columns.tolist(), \
                    self.signalType,\
                    temp_signal_unit,\
                    self.base_type,  \
                    self.base_unit)
    
    def values(self, name=None):
        if name is None:
            return self.df.as_matrix()
        signals_to_return = self.getSignalNames(name)
        if len(signals_to_return) == 0:
            return array([])
        elif len(signals_to_return) == 1:
            return self.df.loc[:, signals_to_return[0]].as_matrix()
        else:
            return self.df.loc[:, signals_to_return].as_matrix()
    
    def __array__(self):
        return self.values()
    
    def __array_wrap__(self, *args):
        ''' This function wraps the result of a numpy function call
        in a Vibrapy object.
        '''
        return_signal = args[0]
        function_name = args[1][0].__str__().split("'")[1]
        original_signal = args[1][1][0]

        signal_names,temp_base,temp_signal_type,temp_signal_unit,temp_base_type,temp_base_unit = original_signal._getSignalMetadata()
        signal_names = ['{}({})'.format(function_name, i) for i in signal_names]

        return buildSignalFromContituents(return_signal,\
                            temp_base,       \
                            signal_names, \
                            temp_signal_type,\
                            temp_signal_unit,\
                            temp_base_type,  \
                            temp_base_unit)
        return args[1]
    
    def base(self):
        return array(self.df.index)
    
    def getSignalNames(self, name_string, reverse_role=False):
        if types.isStr(name_string):
            name_string    = name_string.strip()
            if name_string == '':
                if not reverse_role:
                    return self.df.columns.tolist()
                else:
                    return []
            else:
                signal_names = []
                name_string_list = name_string.split(',')
                for i in name_string_list:
                    if i.strip() in self.df.columns.tolist():
                        signal_names.append(i.strip())
                    else:
                        pass
                        #if for_setitem == False:
                        #warnings.warn('There is no signal with the name {}.  Skipping.'.format(i.strip()))
            return signal_names
        elif types.isInt(name_string):
            if name_string >= -self.shape[1] and\
               name_string <= self.shape[1]:
                return [self.df.columns.tolist()[name_string]]   
            else:
                warnings.warn('Integer out of range.  No valid signal selected')
                return []
        return []
    
    def plot(self, name='', desig='', hold=False, modfunc=None, args={}):
        vibrapy_plot.plot(self, name=name, desig=desig,\
         hold=hold, modfunc=modfunc, args=args)
    
    def semilogy(self, name='', desig='', hold=False, modfunc=None, args={}):
        vibrapy_plot.semilogy(self, name=name, desig=desig,\
         hold=hold, modfunc=modfunc, args=args)
    
    def bar(self, name='', desig='', hold=False, modfunc=None, args={}):
        vibrapy_plot.bar(self, name=name, desig=desig,\
         hold=hold, modfunc=modfunc, args=args)
    
    def _getSignalMetadata(self):
        
        ''' A function used to return a signal's metadata for use in a 
        signal constructor.
        '''
        signal_names     = list(self.df.columns)
        temp_base        = copy.deepcopy(self.df.index.values)
        temp_signal_type = copy.deepcopy([self.signalType[i] for i in signal_names])
        temp_signal_unit = copy.deepcopy([self.signalUnit[i] for i in signal_names])
        temp_base_type   = copy.deepcopy(self.base_type)
        temp_base_unit   = copy.deepcopy(self.base_unit)
        return signal_names, temp_base, temp_signal_type, temp_signal_unit, temp_base_type, temp_base_unit
    
    def flatten(self):
        '''
        A function used to flatten a signal.
        '''
        signal_names, temp_base, temp_signal_type, temp_signal_unit, temp_base_type, temp_base_unit = self._getSignalMetadata()
        matrix_flat = atleast_2d(self.values().flatten()).T
        return  buildSignalFromContituents(matrix_flat,\
                    arange(len(matrix_flat)),\
                    [signal_names[0] + ' - ' + signal_names[-1]], \
                    temp_signal_type[0],\
                    temp_signal_unit[0],\
                    temp_base_type,  \
                    temp_base_unit)
    
    def reshape_last(self, signals):
        ''' A function used to reshape a Signal. 

        Parameters
        ----------

        signals : The number of signals that the Signal should contain. Some rows might dissapear
        as the Signal's does not necassarily contain enough values.  
        '''
        values_temp = self.values().flatten()
        base_temp   = self.base()
        signal_names, temp_base, temp_signal_type, temp_signal_unit, temp_base_type, temp_base_unit = self._getSignalMetadata()
        N = len(values_temp)
        M = int(floor(N/signals))
        values_temp = values_temp[:M*signals].reshape(-1, signals)
        base_temp = base_temp[:M*signals].reshape(-1, signals)
        return  buildSignalFromContituents(values_temp,\
                    base_temp[:,0],\
                    [signal_names[0]+'_1'], \
                    temp_signal_type[0],\
                    temp_signal_unit[0],\
                    temp_base_type,  \
                    temp_base_unit)
    
    def changeNames(self, key_dict=''):
        
        ''' A function that renames a signal name. 

        Parameters
        ----------
        key_dict : A dictionary where each key-value pair is the old_name:new_name. If
            key_dict is a string, brand new names are created using getLabelsFromArg(_signalNames, number_of_signals).
        '''
        signal_names, temp_base, temp_signal_type, temp_signal_unit, temp_base_type, temp_base_unit = self._getSignalMetadata()
        
        if isinstance(key_dict, str):
            new_names = getLabelsFromArg(key_dict, len(signal_names))
            # New names have been obtained. Create new key_dict
            key_dict = {i:j for i, j in zip(signal_names,new_names)}
        if not isinstance(key_dict, dict):
            raise ValueError('The new signal name must be either a string or a dictionary') 
        # Change each signal name
        for i, j in key_dict.items():
            if i in signal_names:
                self.signalUnit[j] = self.signalUnit.pop(i)
                self.signalType[j] = self.signalType.pop(i)
                self.df.rename(columns={i:j}, inplace=True)

    def changeSignalUnits(self, key_dict=''):
        
        ''' A function that changes the signal units. 

        Parameters
        ----------
        key_dict : A dictionary where each key-value pair is the old_name:new_unit. If
            key_dict is a string, all units are set to the new unit.
        '''
        signal_names, temp_base, temp_signal_type, temp_signal_unit, temp_base_type, temp_base_unit = self._getSignalMetadata()

        if isinstance(key_dict, str):
            key_dict = {i:key_dict for i in signal_names}
            # New names have been obtained. Create new key_dict
        if not isinstance(key_dict, dict):
            raise ValueError('The new signal unit must be either a string or a dictionary') 
        # Change each signal name
        for i, j in key_dict.items():
            if i in signal_names:
                self.signalUnit[i] = j

    def changeSignalTypes(self, key_dict=''):
        
        ''' A function that changes the signal types. 

        Parameters
        ----------
        key_dict : A dictionary where each key-value pair is the old_name:new_type. If
            key_dict is a string, all types are set to the new type.
        '''
        signal_names, temp_base, temp_signal_type, temp_signal_unit, temp_base_type, temp_base_unit = self._getSignalMetadata()

        if isinstance(key_dict, str):
            key_dict = {i:key_dict for i in signal_names}
            # New names have been obtained. Create new key_dict
        if not isinstance(key_dict, dict):
            raise ValueError('The new signal unit must be either a string or a dictionary') 
        # Change each signal name
        for i, j in key_dict.items():
            if i in signal_names:
                self.signalType[i] = j

    def changeBaseUnit(self, value='s'):
        
        ''' A function that changes the base unit. 

        Parameters
        ----------
        key_dict : The new base unit as a string.
        '''
        self.base_unit = value

    def changeBaseType(self, value='Temporal'):
        
        ''' A function that changes the base type. 

        Parameters
        ----------
        key_dict : The new base type as a string.
        '''
        self.base_type = value

    def alterBase(self, func):

        ''' A function used to later the base values of the signal.

        Parameters
        ----------

        func : A function that will be called as such: df.index = func(array(df.index)). 
        This function creates a new base.
        '''
        self.df.index = func(array(self.df.index))
class Signal(oneDSignalBase):
    def __init__(self, matrix, base=None, signalNames=None,signalType=None, signalUnit=None, base_type=None, base_unit=None, verbose=False):
        super().__init__()
        self.verbose = verbose

        self.trackTime('__init__-------------------------------{')
        _base           = None
        _signalNames    = []
        _signalType     = {}
        _signalUnit     = {}
        _base_type      = None
        _base_unit      = None
        self.trackTime()
        if not types.isIter(matrix):
            matrix = [matrix]
        self.trackTime()
        if types.isPand(matrix):
            _base  = list(matrix.index)
            if isinstance(matrix, Series):
                if matrix.name:
                    _signalNames.append(matrix.name)
            else:
                _signalNames += matrix.columns.tolist()
            matrix = matrix.as_matrix()
        self.trackTime()
        if types.isDict(matrix):
            for i, j in matrix.items():
                if not types.isIter(j):
                    raise ValueError('The values for "{}" in '.format(i)+\
                     'the matrix dictionary is not an iterable.')
                if types.isDict(j):
                    raise ValueError('The values for "{}" in '.format(i)+\
                     'the matrix dictionary must be a list or numpy array.')
            iterDepth = 1
        else:
            iterDepth = types.getIterDepth(matrix)
        self.trackTime()
        if iterDepth > 2:
            raise ValueError('Matrix has a depth larger than 2 and can '+\
                'therefore not be used to create a Signal')
        self.trackTime()
        if types.isDict(matrix):
            # Instantiate object if input is a dictionary
            _signalNames    = []
            matrix_list     = []
            for i, j in matrix.items():
                matrix_list.append(j)
                _signalNames.append(i)
            matrix = matrix_list
        self.trackTime()
        if types.isList(matrix):
            iterDepth = types.getIterDepth(matrix)
            ## If matrix is a list, cast it into the correct matrix form.
            if iterDepth == 1:
                if types.isEmptyList(matrix):
                    matrix = zeros([0,1])
                else:
                    matrix = atleast_2d(matrix).T
            if iterDepth == 2:
                if len(matrix)==1 and types.isEmptyList(matrix[0]):
                    matrix = zeros([0,1])
                else:
                    minLen_lys = [len(i) for i in matrix]
                    if sum(array(minLen_lys) == min(minLen_lys)) ==\
                     len(matrix):
                        matrix = atleast_2d(matrix).T
                    else:
                        warnings.warn('Not all of the signals are the same length.  Truncating al signals to {} values'.format(min(minLen_lys)))
                        matrix = atleast_2d([i[:min(minLen_lys)]\
                    for i in matrix]).T
        elif types.isND(matrix):
            iterDepth = types.getIterDepth(matrix)
            if iterDepth == 1:
                matrix = atleast_2d(matrix).T
            else:
                # Assume the matrix is already oriented correctly and \
                # that it is a proper square shape
                pass
        self.trackTime()
        if not signalNames is None:
            _signalNames = getLabelsFromArg(signalNames, matrix.shape[1])
        else:
            _signalNames = getLabelsFromArg(_signalNames, matrix.shape[1])
        self.trackTime()
        if not base is None:
            if types.isNum(base):
                _base = arange(matrix.shape[0]) * base
            elif types.isND(base) or types.isList(base) or\
             types.isPandIndex(base):
                if types.getIterDepth(base) == 1 and\
                  len(base) == matrix.shape[0]:
                    _base   = array(base)
                else:
                    raise ValueError('Base is not the same length'+\
                    ' as the signal')
            else:
                raise ValueError('Base is not a number or array')
        self.trackTime()
        if _base is None:
            _base = arange(matrix.shape[0])
        self.trackTime()
        if isinstance(signalType, str):
            for i in _signalNames:
                _signalType[i] = signalType
        elif types.isDict(signalType):
            for i, j in signalType.items():
                if i in _signalNames:
                    _signalType[i] = j
        elif types.isList(signalType):
            for i, j in zip(_signalNames,signalType):
                _signalType[i] = j
        for i in _signalNames:
            if not i in _signalType.keys():
                _signalType[i] = 'Voltage'
        self.trackTime()
        if isinstance(signalUnit, str):
            for i in _signalNames:
                _signalUnit[i] = signalUnit
        elif types.isDict(signalUnit):
            for i, j in signalUnit.items():
                if i in _signalNames:
                    _signalUnit[i] = j
        elif types.isList(signalUnit):
            for i, j in zip(_signalNames,signalUnit):
                _signalUnit[i] = j
        for i in _signalNames:
            if not i in _signalUnit.keys():
                _signalUnit[i] = 'V'
        self.trackTime()
        if not base_unit is None:
            if not types.isStr(base_unit):
                raise types.isStr(base_unit, 'base_unit')
            else:
                _base_unit = base_unit
        else:
            _base_unit = 's'
        self.trackTime()
        if not base_type is None:
            if not types.isStr(base_type):
                raise types.isStr(base_type, 'base_type')
            else:
                _base_type = base_type
        else:
            _base_type = 'Temporal'
        self.trackTime()
        self.df = DataFrame(matrix, index=_base, columns=_signalNames)
        self.signalUnit = _signalUnit
        self.signalType = _signalType
        self.base_type  = _base_type
        self.base_unit  = _base_unit
        self.shape      = (len(self.df.index), \
                           len(self.df.columns.tolist()))
        self.trackTime('}-------------------------------------__init__')
def buildSignalFromContituents(temp_matrix,\
                    temp_base,       \
                    signal_names, \
                    temp_signal_type,\
                    temp_signal_unit,\
                    temp_base_type,  \
                    temp_base_unit):

    return Signal(temp_matrix,\
                    temp_base,       \
                    signal_names, \
                    temp_signal_type,\
                    temp_signal_unit,\
                    temp_base_type,  \
                    temp_base_unit)