'''
Tests for the vibrapy base class
'''

from scipy.io import loadmat
from scipy.integrate import cumtrapz
from pandas import DataFrame, Series
from numpy import gradient, array, linspace, sin, pi, cos, ones
import vibrapy.base as VB
import time

from matplotlib import pyplot as plt

def testAngularSignal():
    data = loadmat('./data/testPreprocess/2015_06_08_T2_ramp_met_sin.mat')
    signal = data['Track2'][0][:int(len(data['Track2'][0]))]
    x_inc  = data['Track2_X_Resolution'][0,0]

    from vibrapy.preprocess import extractZeroCrossing

    zero_crossings = extractZeroCrossing.threshhold(signal, dt=x_inc, disp=False)
    testSignal = VB.angularSignal(zero_crossings, N=79, M_cal=13)
    for arg, j in zip(['wrap_lim'], [[1, 2, 3]]):
        signals, time_now = testSignal('IAP|equiBase|wrap')
        plt.plot(testSignal.df.index, testSignal.df['IAP'], 'o-', ms=20,color='r', alpha=.5)
        plt.plot(time_now, abs(signals[:, 0]), 'o-',color='b')
        plt.plot(time_now, signals[:, 0], 'o-',color='b')
        plt.show()

def testSignal():
    t = linspace(0,20, 100)
    x = sin(10*2*pi*t) + 5
    y = sin(27.6*2*pi*t)
    testSignal   = VB.Signal([x, y], t)
    #testSignal['x'].plot(desig='Oorsprinklik', hold=True)
    #testSignal('polyNormalize')['x'].plot(desig='Genormaliseer')
    #print(testSignal.values('x').shape, testSignal.values().shape)
    testSignal.plot()

testSignal()