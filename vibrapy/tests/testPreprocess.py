from vibrapy import preprocess as PP 
from scipy.io import loadmat
import os.path

#from matplotlib import pyplot as plt
#from numpy import ediff1d

data_dir             = './data/testPreprocess/'
shaft_encoder_signal = '2015_06_08_T2_ramp_met_sin.mat'

def testExtractZeroCrossingThreshold():
    data = loadmat(os.path.join(data_dir, 
    shaft_encoder_signal))
    signal = data['Track2'][0][:int(len(data['Track2'][0])/20)]
    x_inc  = data['Track2_X_Resolution']
    #plt.plot(signal)
    #plt.show()

    PP.threshhold(signal, disp=False)
    #PP.threshhold(signal, threshhold='0.25')
    #PP.threshhold(signal, threshhold=2.5)
    #PP.threshhold(signal, threshhold='0.5',lower_lim=1, higher_lim=2)
    #PP.threshhold(signal, dt = x_inc[0,0])
    #PP.threshhold(signal, threshhold=2.5, lower_lim=1, higher_lim=2)
    #PP.threshhold(signal, threshhold='a', lower_lim='d')
    #PP.threshhold(signal, threshhold='a')
    #PP.threshhold(signal, gradient_positive=False)
    #plt.plot(PP.threshhold(signal, dt = x_inc[0,0])[:-1], 1/ediff1d(PP.threshhold(signal, dt = x_inc[0,0]))/79.0 * 60)
    #plt.show()
testExtractZeroCrossingThreshold()