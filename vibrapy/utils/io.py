"""
This module contains io functions for reading and writing signals.

Authors: David Hercules Diamond

License: MIT

"""

from pandas   import read_csv as pand_read_csv
from numpy    import array, ndarray
from vibrapy  import Signal
from scipy.io import loadmat

import vibrapy as vp

from vibrapy.utils import types

def read_csv(filepath, base=None, signalNames=None, signalType=None, signalUnit=None, base_type=None , base_unit=None, base_colname=None, **kwargs):
    ''' A function that reads a CSV file and creates a Vibrapy signal from the csv file.

    Parameters
    ----------
    filepath : string. The path to the file to read.

    base : The base variable to be passed to the Vibrapy constructor. See Vibrapy.base.Signal.Signal. Cannot be specified
        along with base_colname.

    signalNames : The signalNames variable to be passed to the Vibrapy constructor. See Vibrapy.base.Signal.Signal.

    signalType : The signalType variable to be passed to the Vibrapy constructor. See Vibrapy.base.Signal.Signal.

    signalUnit : The signalUnit variable to be passed to the Vibrapy constructor. See Vibrapy.base.Signal.Signal.

    base_type : The base_type variable to be passed to the Vibrapy constructor. See Vibrapy.base.Signal.Signal.

    base_unit : The base_unit variable to be passed to the Vibrapy constructor. See Vibrapy.base.Signal.Signal.

    base_colname : The column name of the column in the read csv that corresponds to the base of the signal. Defaults to
        none if there are base values to use for the constructor. This attribute cannot be specified along with base.

    **kwargs : Additional keyword arguments to pass to the pandas.read_csv function.


    Returns
    -------

    The constructed vibrapy signal.

    '''
    df = pand_read_csv(filepath, **kwargs)
    if base_colname:
        if base:
            raise ValueError('Specification conflict. You cannot specify both the base and base_colname attributes.')
        if base_colname in list(df.columns):
            base = array(df[base_colname])
            df.drop(base_colname, axis=1, inplace=True)
        else:
            raise ValueError("The column name {} is not in the DataFrame".format(base_colname))
    if signalNames is None:
        signalNames = list(df.columns)
    return Signal(df.values, base, signalNames, signalType, signalUnit, base_type, base_unit)

def read_mat_metadata(filename, return_raw=False, return_first=False):

    ''' A function that returns a MATLAB file's dictionary keys. 
    The intent is to have this function read ONLY the keys and not 
    the values. This will be implemented in future. Currently this
    function reads the entire MATLAB file and returns the keys after
    deleting the values.
    
    Parameters
    ----------
    
    filename : The directory to the filename.
    
    return_raw: Whether to return only the dictionary keys or the entire dictionary.
        False by default.

    return_singles : Boolean. Whether or not to return the first value of each key. If return_raw is True, 
        this parameter is ignored. False by default.
    
    Returns
    -------
    
    The MATLAB file keys.
    '''
    data = loadmat(filename)
    data_keys = data.keys()
    if return_raw == True:
        return data
    elif return_first == True:
        first_vals = {}
        for key in data_keys:
                try:
                    first_vals[key] = array(data[key]).flatten()[0]    
                except Exception as e:
                    print('Key {} does not appear to be an array. Returning entire key'.format(key))
                    first_vals[key] = data[key]
        return first_vals
    del data
    return sorted(list(data_keys))

def read_mat(filename, variable_names=None,
             base=None,
             signalType=None,
             signalUnit=None,
             base_type=None,
             base_unit=None,
             base_colname=None,
             skip_single=True, **kwargs):

    ''' A function that reads a MATLAB file and returns a Vibrapy signal.
    Note: This function cannot yet import MATLAB multidimensional matrices.
    Each signal must have its own key name.
    
    Parameters
    ----------
    
    filename : The directory to the filename.
    
    variable_names : List of variables names to import. Can also be a single string.

    base : The base variable to be passed to the Vibrapy constructor. See Vibrapy.base.Signal.Signal. Cannot be specified
        along with base_colname.

    signalType : The signalType variable to be passed to the Vibrapy constructor. See Vibrapy.base.Signal.Signal.

    signalUnit : The signalUnit variable to be passed to the Vibrapy constructor. See Vibrapy.base.Signal.Signal.

    base_type : The base_type variable to be passed to the Vibrapy constructor. See Vibrapy.base.Signal.Signal.

    base_unit : The base_unit variable to be passed to the Vibrapy constructor. See Vibrapy.base.Signal.Signal.

    base_colname : The dictionary keyword to use for the base constructor column. 
    This attribute cannot be specified along with base.

    skipsingle : Whether or not to skip single values for the import.

    **kwargs : Additional keyword arguments to pass to the pandas.read_csv function.
    
    
    Returns
    -------
    
    Vibrapy signal containing the values.
    '''
    data = loadmat(filename)
    if variable_names:
        if types.isStr(variable_names): variable_names = [variable_names] # If multiple variable names have been specified
    else:
        variable_names = []
        for i in list(data.keys()):
            if i not in ['__globals__', '__header__', '__version__']:
                variable_names.append(i)
    data_dict = {}
    for name in variable_names:
        data_dict[name] = array(data[name]).flatten()
        if skip_single and (len(data_dict[name]) == 1): del data_dict[name]
    if base_colname:
        if base_colname in list(data.keys()):
            base = array(data[base_colname]).flatten()
            if len(base) ==1:
                base = base[0]
    return vp.Signal(data_dict, base,None, signalType, signalUnit, base_type, base_unit)