'''
Module containing functinos to test for variable types.
======================================================
Authors: David Hercules Diamond

License: MIT

'''
from numpy import int, int16, int8, int32, int64, intc, float, float64, float32, float16, ndarray, array
from numpy import bool_ as npbool, unique
from vibrapy.base import Signal as VS
from pandas import DataFrame, Series, Index,to_numeric
from time import time
def isVibrapy(       series, error=''):
    '''
    Function to determine if series is a Vibrapy signal object.
    '''
    if not error is '':
        raise ValueError('The variable "{}" is not a valid vibrapy signal object.'.format(error))
    if isinstance(series, VS.Signal):
        return True
    return False
def isNum(           num, error=''):
    '''
    Function to determine if num is a float or integer.
    '''
    if not error is '':
        raise ValueError('The variable "{}" with current value "{}" is not a valid number.'.format(error, num))
    if type(num) in [int, int16, int8, int32, int64, intc, float, float64, float32, float16]:
        return True
    return False
def isNatural(       num, error=''):
    '''
    Function to determine if a number is a natural number (integer and larger than 0)
    '''
    if not error is '':
        raise ValueError('The variable "{}" with current value "{}" is either not an integer or larger than 1.'.format(error, num))
    if type(num) in [int, int16, int8, int32, int64, intc]:
        if num > 0:
            return True
    return False
def isInt(           num, error=''):
    '''
    Function to determine if a number is an integer.
    '''
    if not error is '':
        raise ValueError('The variable "{}" with current value "{}" is not an integer.'.format(error, num))
    if type(num) in [int, int16, int8, int32, int64, intc]:
        return True
    return False
def isIntnotNull(    num, error=''):
    '''
    Function to determine if a number is an integer and not zero.
    '''
    if not error is '':
        raise ValueError('The variable "{}" with current value "{}" is not an integer or is equal to 0.'.format(error, num))
    if type(num) in [int, int16, int8, int32, int64, intc]:
        if num == 0:
            return False
        return True
    return False
def float_positive(  num, error=''):
    '''
    Function to determine if a number is a valid number and greater or equal to zero.
    '''
    if not error is '':
        raise ValueError('The variable "{}" with current value "{}" is either not a valid number or not positive.'.format(error, num))
    if isNum(num):
        if num > 0:
            return True
    return False
def isPand(          series, error=''):
    '''
    Function to determine if series is a Pandas DataFrame or Series.
    '''
    if not error is '':
        raise ValueError('The variable "{}" is not a valid Pandas Seris or DataFrame.'.format(error))
    if type(series) in [DataFrame, Series]:
        return True
    return False
def isPandIndex(     series, error=''):
    '''
    Function to determine if series is a Pandas Index.
    '''
    if not error is '':
        raise ValueError('The variable "{}" is not a valid Pandas Index.'.format(error))
    if isinstance(series, Index):
        return True
    return False
def isPandSeries(    series, error=''):
    '''
    Function to determine if series is a Pandas Index.
    '''
    if not error is '':
        raise ValueError('The variable "{}" is not a valid Pandas Series.'.format(error))
    if isinstance(series, Series):
        return True
    return False
def isEmptyList(     series, error=''):
    '''
    Function to determine if series is an empty Python list.
    '''
    if not error is '':
        raise ValueError('The variable "{}" is not an empty list.'.format(error))
    if isinstance(series, list):
        if series == []:
            return True
    return False
def isIter(          series, error=''):
    '''
    Function to determine if series is an iterable.
    '''
    if not error is '':
        raise ValueError('The variable "{}" is not a valid iterable.'.format(error))
    try:
        iter(series)
        return True
    except:
        return False
def isBool(          val, error=''):
    '''
    Function to determine if value is boolean.
    '''
    if not error is '':
        raise ValueError('The variable "{}" is not a valid boolean.'.format(error))
    if isinstance(val, bool) or isinstance(val, npbool):
        return True
    return False
def isND(            series, error=''):
    '''
    Function to determine if series is a NDArray.
    '''
    if not error is '':
        raise ValueError('The variable "{}" is not a numpy ndarray.'.format(error))
    if isinstance(series, ndarray):
        return True
    return False
def isList(          series, error=''):
    '''
    Function to determine if series is a Python list.
    '''
    if not error is '':
        raise ValueError('The variable "{}" is not a Python list.'.format(error))
    if isinstance(series, list):
        return True
    return False
def isTupleOfStr(    series, error=''):
    '''
    Function to determine if series is a tuple of pure strings.
    '''
    if not error is '':
        raise ValueError('The variable "{}" is not a tuple of pure strings.'.format(error))
    if isinstance(series, tuple):
        for i in series:
            if not isStr(i):
                return False
        return True
    return False
def isTupleOfSlice(  series, error=''):
    '''
    Function to determine if series is a tuple of pure strings.
    '''
    if not error is '':
        raise ValueError('The variable "{}" is not a tuple of pure slices.'.format(error))
    if isinstance(series, tuple):
        for i in series:
            if not isSlice(i):
                return False
        return True
    return False
def isTupleOfInt(    series, error=''):
    '''
    Function to determine if series is a tuple of integers.
    '''
    if not error is '':
        raise ValueError('The variable "{}" is not a tuple of pure integers.'.format(error))
    if isinstance(series, tuple):
        for i in series:
            if not isInt(i):
                return False
        return True
    return False
def isSlice(         series, error=''):
    '''
    Function to determine if series is a Python list.
    '''
    if not error is '':
        raise ValueError('The variable "{}" is not a slice.'.format(error))
    if isinstance(series, slice):
        return True 
    return False
def isListofStr(     series, error=''):
    '''
    Function to determine if series is a Python list.
    '''
    if not error is '':
        raise ValueError('The variable "{}" is not a Python list of strings.'.format(error))
    if isinstance(series, list):
        for i in series:
            if not isinstance(i, str):
                return False
        return True 
    return False
def isListofInt(     series, error=''):
    '''
    Function to determine if series is a Python list.
    '''

    if not error is '':
        raise ValueError('The variable "{}" is not a Python list of integers.'.format(error))
    if isinstance(series, list):
        try:
            to_numeric(series)
            return True
        except:
            return False
    return False
def isNDorListofInt( series, error='', debug=False):
    '''
    Function to determine if series is an NDarray of List of integers.
    '''
    #Check if the type is boolean
    try:
        if series.dtype == bool:
            return False
    except:
        pass


    if debug: begin = time()
    if debug: print('Now in isNDorListofInt')
    if debug: print(time() - begin)
    if not error is '':
        raise ValueError('The variable "{}" is not an iterable of pure integers.'.format(error))
    if isND(series) or isList(series):
        if debug: print(time() - begin)
        if debug: print('Is ND or is List')
        if debug: print(time() - begin)
        if debug: print('Checking of this is a boolean list')
        if debug: print(time() - begin)
        if array(series).dtype == bool:
            if debug: print('List is Boolean, returning False')
            if debug: print(time() - begin)
            return False
        try:
            if debug: print('Trying to convert to numeric')
            if debug: print(time() - begin)
            to_numeric(series)
            if debug: print('Finished converting to numeric. Returning True')
            if debug: print(time() - begin)
            return True
        except:
            if debug: print('Could not convert list. Returning False')
            if debug: print(time() - begin)
            return False
    if debug: print(time() - begin)
    return False
def isNDorListofBool(series, error='', debug=False):
    '''
    Function to determine if series is an NDarray or list of boolean.
    '''
    if debug:
        print('isNDorListofBool')
        begin = time()
    
    if not error is '':
        raise ValueError('The variable "{}" is not an iterable of pure boolean.'.format(error))
    
    # Try checking if this is an ND array of type boolean
    try:
        if series.dtype == bool:
            return True
        else:
            False
    except:
        return False

    if isND(series) or isList(series):
        if debug:
            print('is ND series or list')
            if debug: print(time() - begin)
            print('Type of series is {}'.format(type(series)))
            if debug: print(time() - begin)
            print('First 5 values is {}'.format(series[0:5]))
            if debug: print(time() - begin)
            print('Numpy array is type {}. {}'.format(array(series).dtype, array(series).dtype==bool))
            if debug: print(time() - begin)
        if array(series).dtype == bool:
            if debug: print('Returning True')
            if debug: print(time() - begin)
            return True
        
        unique_vals = unique(series)

        if debug:
            print('Unique vals is ', unique_vals)
            if debug: print(time() - begin)
        if (True in unique_vals) and (False in unique_vals) and (len(unique_vals)==2):
            if debug: print('Returning True')
            if debug: print(time() - begin)
            return True
        if debug: print('Returning False')
        if debug: print(time() - begin)
        return False
    if debug: print('Returning False')
    if debug: print(time() - begin)
    return False
def isDict(        series, error=''):
    '''
    Function to determine if series is a dictionary.
    '''
    if not error is '':
        raise ValueError('The variable "{}" is not a Python dictionary.'.format(error))
    if isinstance(series, dict):
        return True
    return False
def getIterDepth(  series, error='', safe=False):
    '''
    Function to determine the depth of an iterable.

    safe : If True, maximum iterdepth is 5.
    '''
    n = 1
    if iterNotEmpty(series):
        val = series[0]
    else:
        return n

    while isIter(val):
        n += 1
        if safe is True and n == 5:
            return 5
        if iterNotEmpty(val):
            val = val[0]
        else:
            return n        
    return n
def iterNotEmpty(  series, error=''):
    '''
    Function to determine if an iterable is empty.
    '''
    if not error is '':
        raise ValueError('The variable "{}" is empty.'.format(error))
    if len(series) != 0:
        return True
    return False
def isStr(         val, error=''):
    '''
    Function to determine if a value is a string.
    '''
    if not error is '':
        raise ValueError('The variable "{}" with value {} is not a valid string.'.format(error, val))
    if isinstance(val, str):
        return True
    return False

def isNotNone(val, error=''):
    
    if not error is '':
        raise ValueError('The variable "{}" with value {} is a None.'.format(error, val))

    if not (val is None):
        return True
    return False

def isNone(val, error=''):
    if not error is '':
        raise ValueError('The variable "{}" with value {} is not None.'.format(error, val))
    if val is None:
        return True
    return False

# Base58 string for name selection
BASE58     = '123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ'
