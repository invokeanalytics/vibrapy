'''
Module containing general signal processing functions for Vibrapy.
==================================================================
Authors: David Hercules Diamond

License: MIT

All functions must take arguments and return certain arguments:

Parameters
----------
matrix : NDArray.  A matrix containing signals in the temporal domain.  Each column must be a different signal.

base : NDArray.  The temporal base of the signal.

signalNames : List containing the signal name for each signal in matrix.

signalType : List containing the types for the signals in matrix.

signal_unit : List containing the units for each signal in signalNames.

base_type : string.  A function specifying the base type.  If base_type
is not "Temporal" the function simply returns the inputs along with a 
warning.

base_unit: string.  Function value giving the base_unit.

signal : Signal or angularSignal.  The signal instance from which this
function was called. 

args : dict containing additional arguments.

Return
------
Transformed values for the following variables:

matrix

base

signalNames 

signalType 

signal_unit 

base_type 

base_unit 
'''
import warnings as __warnings__

import numpy as __np__
from scipy.interpolate import interp1d as __interp1d__
from scipy.integrate import cumtrapz as __spcumtrapz__
from vibrapy.utils import types as __types__
from scipy.signal import butter as __butter__, lfilter as __lfilter__
import copy as __copy__

import warnings

class algorithm:
    ''' 
    The base algorithm class.
    '''
    def __init__(self):
        '''
        '''
        self.isAlgorithm = True
        pass


class aveOverRows(algorithm):
    def __init__(self):
        super().__init__()
    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):
        '''
        Function to calculate the average of each row in a signal.
        '''
        if matrix.shape[1] == 1:
            raise ValueError('You cannot calculate the average over rows where only a single signal exists.')
        average = __np__.atleast_2d(__np__.mean(matrix, axis=1)).T

        return average, base, ['Average of {} to {}'.format(signalNames[0], signalNames[-1])], ['Average'], signal_unit[0], base_type, base_unit


class rFFT(algorithm):
    def __init__(self):
        super().__init__()
    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):
        '''
        Function to calculate the positive frequency spectrum of series with
        base.
        '''
        if base_type == 'Temporal':
            original_size = matrix.shape[0]
            matrix = __np__.fft.rfft(matrix, axis=0) / matrix.shape[0] * 2
            base   = __np__.fft.rfftfreq(original_size, base[2] - base[1])
            
            base_type = 'Frequency'

            if base_unit == 's':
                base_unit = 'Hz'
            elif base_unit == 'rad':
                base_unit = '1/rad'
            elif base_unit == 'order':
                base_unit = '1/revolution'
            else:
                __warnings__.warn('I do not understand the unit {}.  Defaulting to s.'.format(base_unit))
            state_changed = True
        else:
            warnings.warn('Input is not temporal.  Returning input paramaters'
            ' unchanged.')
        return matrix, base, signalNames, signalType, signal_unit, base_type, base_unit

class FFT(algorithm):
    def __init__(self):
        super().__init__()
    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):
        '''
        Function to calculate the positive frequency spectrum of series with
        base.
        '''
        if base_type == 'Temporal':
            original_size = matrix.shape[0]
            dt = base[2] - base[1]
            matrix_list = []
            # Perform the FFT
            for i in range(matrix.shape[1]):
                fft_x     = __np__.fft.fft(matrix[:,i]) 
                # Normalise the FFT
                fft_x     =   2*fft_x / original_size
                # Shift the FFT such that the zero frequency term is in the middle 
                fft_x     = __np__.fft.fftshift(fft_x) 
                # Calculate the frequency values
                matrix_list.append(fft_x * 1)

            fft_freqs = __np__.fft.fftfreq(original_size, dt) 
            # Shift the frequency values according to the shifted FFT signal
            fft_freqs = __np__.fft.fftshift(fft_freqs)
            
            matrix = __np__.array(matrix_list).T
            base   = fft_freqs
            
            base_type = 'Frequency'

            if base_unit == 's':
                base_unit = 'Hz'
            elif base_unit == 'rad':
                base_unit = '1/rad'
            elif base_unit == 'order':
                base_unit = '1/revolution'
            else:
                __warnings__.warn('I do not understand the unit {}.  Defaulting to s.'.format(base_unit))
            state_changed = True
        else:
            warnings.warn('Input is not temporal.  Returning input paramaters'
            ' unchanged.')
        return matrix, base, signalNames, signalType, signal_unit, base_type, base_unit


class PSD(algorithm):
    ''' Calculates the power spectral density of the incoming vibrapy
    signal.
    '''
    def __init__(self):
        super().__init__()
    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):
        '''
        Function to calculate the positive frequency spectrum of series with
        base.
        '''
        if base_type == 'Frequency':
            matrix = __np__.abs(matrix)**2
            signalType = ['PSD()'.format(i) for i in signalNames]
            signal_unit = [ '|' + i + r'|$^{2}$' for i in signal_unit]
        else:
            warnings.warn('Input is not frequency.  Returning input paramaters'
            ' unchanged.')
        return matrix, base, signalNames, signalType, signal_unit, base_type, base_unit

class equiBase(algorithm):
    '''
    Function to interpolate the signals to an equidistant base. Increases 
    the number of samples by 2.56.

    Parameters
    ----
    base_min : The minimum base value.  

    base_max : The maximum base value.

    base_len : The length of the base function.  This argument takes
    priority over base_inc if specified.

    base_inc : The factor with which to increase upon the current length.
    This argument defaults to 2.56 and takes priority over base_len if
    not specified.

    kind : The kind of interpolation in the interp1d function.  
    Defaults to 'quadratic'.
    '''
    def __init__(self, base_min=None, base_max=None, base_len=None, base_inc=None, kind=None):
        super().__init__()
        self.base_min = base_min
        self.base_max = base_max
        self.base_len = base_len
        self.base_inc = base_inc
        self.kind = kind
    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):
        if __types__.isNotNone(self.base_min):
            base_min = self.base_min
        else:
            base_min = base.min()

        if __types__.isNotNone(self.base_max):
            base_max = self.base_max
        else:
            base_max = base.max()

        if __types__.isNotNone(self.base_len):
            base_len = self.base_len
        else:
            if __types__.isNotNone(self.base_inc):
                if __types__.float_positive(self.base_inc):
                    base_inc = self.base_inc
                else:
                    __types__.float_positive(self.base_inc, 'base_inc')
            else:
                base_inc = 2.56
            base_len = int(len(base) * base_inc)

        if __types__.isNotNone(self.kind):
            if self.kind in ['linear', 'nearest', 'zero', 'slinear', 'quadratic', 'cubic']:
                kind = self.kind
            else:
                __warnings__.warn('The kind "{}" is not in the allowed types, namely: linear, nearest, zero, slinear, quadratic, cubic.  Defaulting to "quadratic"'.format(self.kind))
                kind = 'linear'
        else:
            kind = 'linear'

        if __types__.isNatural(base_len) == False:
            __types__.isNatural(base_len, 'base_len')
        if __types__.isNum(base_min) == False:
            __types__.isNum(base_min, 'base_min')
        if __types__.isNum(base_max) == False:
            __types__.isNum(base_max, 'base_max')

        # State has changed.  Try to perform interpolation.
        try:
            base_old   = __copy__.deepcopy(base)
            matrix_old = __copy__.deepcopy(matrix)
            base       = __np__.linspace(base_min, base_max, base_len)
            matrix     = __np__.zeros([len(base), len(signalNames)])
            for i, j in enumerate(signalNames):
                matrix[:, i] = __interp1d__(base_old, matrix_old[:, i],\
                kind=kind)(base)
            state_changed = True
        except:
            warnings.warn('Input could not be interpolated.'
                '  Returning input paramaters unchanged.')
            matix = matrix_old
            base  = base_old
        return matrix, base, signalNames, signalType, signal_unit, base_type, base_unit


class interp(algorithm):
    '''
    Function to interpolate the signals at specific time values t.

    args
    ----
    new_base : The base values to interpolate to.

    kind : The kind of interpolation in the interp1d function.  
    Defaults to 'linear'.

    check_range : bool.  A value indicating whether the base should be
    filtered according to the values that are within the interpolation range.
    This makes the function robust but can lead to unexpected returns.

    '''
    def __init__(self, new_base=None, check_range=None, kind=None):
        super().__init__()
        if __types__.isVibrapy(new_base):
            if new_base.shape[1] > 1:
                raise ValueError('new_base must be 1d, currently is {}d.'.format(new_base.shape[1]))
            self.new_base = new_base.values().flatten()
        else:
            self.new_base    = new_base
        self.check_range = check_range
        self.kind        = kind

    def process(self, matrix, base, signalNames, signalType,\
        signal_unit, base_type, base_unit,\
        signal):

        if __types__.isNone(self.new_base):
            raise ValueError('No argument for new_base passed.')
        new_base = __np__.array(self.new_base)
        if not __types__.isIter(new_base):
            types.isIter(new_base, 'new_base')

        if __types__.isNotNone(self.check_range):
            check_range = self.check_range
        else:
            check_range = False
        
        if not __types__.isBool(check_range):
            __types__.isBool(check_range, 'check_range')
        
        if __types__.isNotNone(self.kind):
            if self.kind in ['linear', 'nearest', 'zero', 'slinear', 'quadratic', 'cubic']:
                kind = self.kind
            else:
                __warnings__.warn('The kind "{}" is not in the allowed types, namely: linear, nearest, zero, slinear, quadratic, cubic.  Defaulting to "quadratic"'.format(self.kind))
                kind = 'linear'
        else:
            kind = 'linear'
        base_old   = __copy__.deepcopy(base)
        matrix_old = __copy__.deepcopy(matrix)
        base       = new_base
        if check_range:
            base   = base[(base >= base_old.min()) &\
                     (base <= base_old.max())]
        matrix     = __np__.zeros([len(base), len(signalNames)])
        for i, j in enumerate(signalNames):
            matrix[:, i] = __interp1d__(base_old, matrix_old[:, i],\
            kind=kind)(base)
        state_changed = True
        return matrix, base, signalNames, signalType, signal_unit, base_type, base_unit


class wrap(algorithm):
    '''
    Function to wrap a signal between 0 and a set maximum value.

    args
    ----

    wrap_lim : The wrap limit.  Defaults to 2 pi.

    '''
    def __init__(self, wrap_lim=None):
        super().__init__()
        self.wrap_lim = wrap_lim
    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):
        '''
        Function to wrap a signal between 0 and a set maximum value.

        args
        ----

        wrap_lim : The wrap limit.  Defaults to 2 pi.

        '''

        if __types__.isNone(self.wrap_lim):
            wrap_lim = 2 * __np__.pi
        else:
            wrap_lim = self.wrap_lim
        if not __types__.isNum(wrap_lim):
            __types__.isNum(wrap_lim, 'wrap_lim')
        try:
            matrix_old = __copy__.deepcopy(matrix)
            matrix     = __np__.zeros([len(base), len(signalNames)])
            for i, j in enumerate(signalNames):
                matrix[:, i] = matrix_old[:, i] % wrap_lim
            state_changed = True
        except:
            __warnings__.warn('Input could not wrapped.')
        return matrix, base, signalNames, signalType, signal_unit, base_type, base_unit


class normalize(algorithm):
    '''
    Function to normalize a Signal by subtracting each signal's average .
    '''
    def __init__(self):
        super().__init__()
    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):
        matrix -= __np__.average(matrix, axis=0)
        return matrix, base, signalNames, signalType, signal_unit, base_type, base_unit


class polyNormalize(algorithm):
    '''
    Function to normalize a Signal by subtracting each a polynomial fit from each.\
    signal.  Default polynomial order is 2.
    '''
    def __init__(self, degree=None):
        super().__init__()
        self.degree = degree
    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):

        if __types__.isNotNone(self.degree):
            degree= self.degree
        else:
            degree = 2
        if __types__.isInt(degree):
            if not degree >=0:
                raise ValueError('Degree must larger or equal than one.')
        else:
            __types__.isInt(degree, 'degree')
        for i in range(matrix.shape[1]):
            matrix[:, i] -= __np__.polyval(__np__.polyfit(base, matrix[:, i], deg=degree), base)
        return matrix, base, signalNames, signalType, signal_unit, base_type, base_unit
class polySmooth(algorithm):
    '''
    A class that allows smooths a time signal by fitting a polynomial through
    the signal.
    '''
    def __init__(self, degree=None):
        super().__init__()
        self.degree = degree
    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):

        if __types__.isNotNone(self.degree):
            degree= self.degree
        else:
            degree = 1
        if __types__.isInt(degree):
            if not degree >=0:
                raise ValueError('Degree must larger or equal than one.')
        else:
            __types__.isInt(degree, 'degree')
        
        for i in range(matrix.shape[1]):
            matrix[:, i] = __np__.polyval(__np__.polyfit(base, matrix[:, i], deg=degree), base)
        return matrix, base, signalNames, signalType, signal_unit, base_type, base_unit


class diff(algorithm):
    '''
    Function to get the consequtive differences of each signal in matrix.
    
    Parameters
    ----------

    infer_base : Boolean. If True, the base is taken as the 
    signal's values 0 to N-1.

    '''
    def __init__(self, infer_base=False):
        super().__init__()
        self.infer_base = infer_base
    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):
        
        temp_matrix = __np__.zeros((matrix.shape[0]-1, matrix.shape[1]))
        
        if self.infer_base == True:
            temp_base   =  matrix[:-1, 0]
            base_type = signalType[0]
            base_unit = signal_unit[0]
        else:
            temp_base   = base[:-1]

        for i in range(temp_matrix.shape[1]):
            temp_matrix[:, i] = __np__.diff( matrix[:, i] )
        return temp_matrix, temp_base, ['diff({})'.format(i) for i in signalNames], ['{} difference'.format(i) for i in signalType], ['\Delta {}'.format(i) for i in signal_unit], base_type, base_unit


class hanning(algorithm):
    '''
    Function to apply a hanning window to temporal data.\
    signal.  Warning!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    This function compensates for lost energy by multiplying with 
    2. This might not be correct.
    '''
    def __init__(self, compensate='amplitude'):
        super().__init__()
        if compensate == 'amplitude':
            self.compensate_factor = 2
        elif compensate == 'energy':
            self.compensate_factor = 1.63
        elif compensate is None:
            self.compensate_factor = 1.0
        else:
            print('Warning: The compensate keyword must be either "amplitude", "energy" or None. Defaulting to a factor of 1.')
            self.compensate_factor = 1.0
    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):
        if base_type == 'Temporal':
            for i in range(matrix.shape[1]):
                matrix[:, i]*=__np__.hanning(matrix.shape[0])
                matrix *= self.compensate_factor
        else:
            __warnings__.warn('A Hanning window multiplication'+\
                ' must be performed on a temporal signal. Window not implemented.')
        return matrix, base, signalNames, signalType, signal_unit, base_type, base_unit


class Abs(algorithm):
    '''
    Return the absolute value of each column in the signal
    '''
    def __init__(self):
        super().__init__()
    def process(self, matrix, base, signalNames, signalType, signal_unit, base_type, base_unit, signal):
        '''
        Return the absolute value of each signal.
        '''
        for i in range(matrix.shape[1]):
            # Convert to the absolute value of each column in matrix
            matrix[:, i] = __np__.abs(matrix[:, i])
        # Return the new signal attributes in the order they were passed through
        return matrix.real, base, signalNames, signalType, ['|{}|'.format(i) for i in signal_unit], base_type, base_unit

class trapz(algorithm):
    ''' Return the integrated signal for the matrix.
    Transforms a 2D matrix into a 1D column vector.  
    
    Parameters
    ----------

    lower: numeric. The lower base limit to start integration from.

    right: numeric. The upper base limit to end integration on.
    '''
    def __init__(self, lower=None, upper=None):
        super().__init__()
        self.lower = lower
        self.upper = upper
    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):
        ''' Return the integrated signal for the matrix.
        Transforms a 2D matrix into a 1D column vector.  
        '''
        if __types__.isNotNone(self.lower):
            lower = self.lower
        else:
            lower = base.min()

        if __types__.isNotNone(self.upper):
            upper = self.upper
        else:
            upper = base.max()
        index     = (base >= lower) & (base <= upper)
        return __np__.atleast_2d(__np__.trapz(matrix[index, :] , base[index], axis=0)),\
               [0],\
               ['Int_{}^{} {}'.format(lower, upper, i) for i in signalNames],\
               [i + ' Integral' for i in signalType],\
               [i + ' Integrated' for i in signal_unit],\
               base_type,\
               base_unit

class cumtrapz(algorithm):
    '''

    Return the cumulative integrated signal for the matrix.  

    Parameters
    ----------

    lower: numeric. The lower base limit to start integration from.

    right: numeric. The upper base limit to end integration on.
    '''
    def __init__(self, lower=None, upper=None):
        super().__init__()
        self.lower = lower
        self.upper = upper
    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):
        if __types__.isNotNone(self.lower):
            lower = self.lower
        else:
            lower = base.min()

        if __types__.isNotNone(self.upper):
            upper = self.upper
        else:
            upper = base.max()
        index     = (base >= lower) & (base <= upper)
        return __spcumtrapz__(matrix[index, :] , base[index], axis=0, initial=0),\
               base[index],\
               ['cumInt_{}^{} {}'.format(lower, upper, i) for i in signalNames],\
               [i + 'Cumulative Integral' for i in signalType],\
               [i + 'Cumulative Integrated' for i in signal_unit],\
               base_type,\
               base_unit

class gradient(algorithm):
    ''' Return the gradient of each signal.  
    '''
    def __init__(self):
        super().__init__()
    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):
        return __np__.gradient(matrix, base,axis=0),\
               base,\
               ['grad({})'.format(i) for i in signalNames],\
               [i + ' gradient'.format(base_type) for i in signalType],\
               [ r'\frac{\mathrm{d}\ '+i+'}{\mathrm{d}\ ' + base_unit +'}' for i in signal_unit],\
               base_type,\
               base_unit

class filt_butter(algorithm):
    ''' A function to perform a bandpass butterworth filter on data.

    Parameters
    ----------

    low: Low cutoff frequency expressed as a fraction of the Nyquist frequency.
        value must be between 0 and 1.
    high: High cutoff frequency expressed as a fraction of the Nyquist frequency.
        value must be between 0 and 1.
    order: Order of Butterworth filter. Must be a positive integer.
    '''
    def __init__(self, low=None, high=None, order=None):
        super().__init__()
        self.low = low
        self.high = high
        self.order = order
    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):
        if __types__.isNotNone(self.low):
            low = self.low
        else:
            low = 0.1

        if __types__.isNotNone(self.high):
            high = self.high
        else:
            high = 0.9

        if __types__.isNotNone(self.order):
            order = self.order
        else:
            order = 2

        if low < 0 or low > 1:
            raise ValueError('low must be between 0 and 1')

        if high < 0 or high > 1:
            raise ValueError('high must be between 0 and 1')

        if order < 0 or int(order) != order:
            raise ValueError('Order must be a positive integer')
        b, a      = __butter__(order, [low, high], btype='band')
        matrix    = __lfilter__(b, a, matrix, axis=0)
        return matrix, base, signalNames, signalType, signal_unit, base_type, base_unit

class filt_ideal(algorithm):
    ''' A function to perform an ideal bandpass filter on data.

    Parameters
    ----------

    low: Low cutoff frequency expressed as a fraction of the Nyquist frequency.
        value must be between 0 and 1.
    high: High cutoff frequency expressed as a fraction of the Nyquist frequency.
        value must be between 0 and 1.

    low_hz : Low cutoff for frequency in Hz.

    high_hz : High cutoff for frequency in Hz.

    filt_type: 'passband' to pass all frequencies between low (or low_hz) and high (or high_hz).
    'stopband' to stop all frequencies between low (or low_hz) and high (or high_hz).
    '''
    def __init__(self, low=None, high=None, low_hz=None, high_hz=None, filt_type='stopband'):
        super().__init__()
        self.low = low
        self.high = high
        self.low_hz = low_hz
        self.high_hz = high_hz
        self.filt_type = filt_type
    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):


        if __types__.isNotNone(self.low_hz):
            low_hz = self.low_hz
        else:
            low_hz = None

        if __types__.isNotNone(self.high_hz):
            high_hz = self.high_hz
        else:
            high_hz = None
        # Check if only one of the two is specified
        if (__types__.isNotNone(low_hz) or __types__.isNotNone(high_hz)) and\
         not (__types__.isNotNone(low_hz) and __types__.isNotNone(high_hz)):
            raise ValueError('You cannot specify only one of low_hz and high_hz. Both must be specified.')
        
        if __types__.isNotNone(self.low):
            low = self.low
        else:
            low = 0.1

        if __types__.isNotNone(self.high):
            high = self.high
        else:
            high = 0.9


        if low < 0 or low > 1:
            raise ValueError('low must be between 0 and 1')

        if high < 0 or high > 1:
            raise ValueError('high must be between 0 and 1')

        matrix_fft = __np__.fft.rfft(matrix, axis=0)
        fft_freqs  = __np__.fft.rfftfreq(matrix.shape[0], base[1]-base[0])

        if __types__.isNotNone(low_hz):
            pass
        else:
            max_freq   =  fft_freqs.max()
            low_hz     = max_freq*low
            high_hz    = max_freq*high
        
        if self.filt_type == 'passband':
            # Get valid frequency terms
            idx_keep   = ~((fft_freqs > low_hz) & (fft_freqs < high_hz))
        elif self.filt_type == 'stopband':
            idx_keep   = ((fft_freqs > low_hz) & (fft_freqs < high_hz))
        else:
            raise ValueError('The type {} is not a valid filter type'.format(self.type))
        # Filter matrix
        matrix_fft[idx_keep, :] = 0
        
        # Inverse FFT
        matrix_return   = __np__.fft.irfft(matrix_fft, axis=0)
        
        # Get maximum length base
        max_len = min([len(base), matrix_return.shape[0]])
        

        return matrix_return[:max_len, :], base[:max_len],\
                 signalNames, signalType,\
                 signal_unit, base_type, base_unit

class alterbase(algorithm):
    ''' A function to alter the base values according to a function.

    Parameters
    ----------
    new_base : A numpy array of the new base values. Is used if specified.
    
    modfunc: A function that is applied to each value in base and the return value
        the new base. Is only used if specified and new_base is not specified.

    new_base_type: String. The new base type.

    new_base_unit : The new base unit.
    '''
    def __init__(self, new_base=None, modfunc=None, new_base_type=None, new_base_unit=None):
        super().__init__()
        self.new_base = new_base
        self.modfunc = modfunc
        self.new_base_type = new_base_type
        self.new_base_unit = new_base_unit
    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):        
        if __types__.isNotNone(self.new_base):
            if len(new_base) == len(base):
                new_base = 1*self.new_base
            else:
                raise ValueError('The new_base must have the same length as the old base')
        elif __types__.isNotNone(self.modfunc):
            try:
                proposed_new_base = __np__.array([self.modfunc(i) for i in base])
            except:
                raise ValueError('''Could not use modfunc to alter base. 
                    Please ensure that modfunc(i) returns a
                     single number where i is a single number''')
            if len(proposed_new_base) == len(base):
                new_base = 1*proposed_new_base
            else:
                raise ValueError('The new_base must have the same length as the old base')

        if __types__.isNotNone(self.new_base_type):
            new_base_type = self.new_base_type
        else:
            new_base_type = base_type

        if __types__.isNotNone(self.new_base_unit):
            new_base_unit = self.new_base_unit
        else:
            new_base_unit = base_unit

        return matrix, new_base,\
                 signalNames, signalType,\
                 signal_unit, new_base_type, new_base_unit

class setIfGreater(algorithm):
    ''' A function used to set the values of a signal that exceeds a certain value.

    Parameters
    ----------
    threshold : numeric. A threshold value that needs to be exceeded.
    
    value: numeric. The value to set the values greater than threshold. 
    '''
    def __init__(self, threshold=0, value=1):
        super().__init__()
        self.threshold = threshold
        self.value = value
    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):        
        matrix_new = __copy__.deepcopy(matrix)
        matrix_new[matrix_new > self.threshold] = self.value
        return matrix_new, base,\
                 signalNames, signalType,\
                 signal_unit, base_type, base_unit