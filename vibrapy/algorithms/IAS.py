'''
Module for containing algorithms to work with Instantaneous Angular Speed (IAS) from zero
crossing times and the encoder geometry.
===============================================
Authors: David Hercules Diamond

License: MIT
'''

import warnings

from numpy import array, identity, ediff1d, sqrt, pi, insert,min, max, sign, diff, arange, insert
from numpy.linalg import inv
from matplotlib import pyplot as plt

import numpy as np
from scipy.sparse.linalg import spsolve
from scipy.sparse import csr_matrix
from vibrapy.utils import types

import warnings as __warnings__

from scipy.interpolate import interp1d as __interp1d__
from scipy.integrate import cumtrapz as __spcumtrapz__
from vibrapy.utils import types as __types__
from scipy.signal import butter as __butter__, lfilter as __lfilter__
import copy as __copy__

class algorithm:
    ''' 
    The base algorithm class.
    '''        
    def __init__(self):
        '''
        '''
        self.isAlgorithm = True
        pass

class getZCross(algorithm):
    
    ''' This algorithm receives a raw tacho-voltage signal and extracts the
        zero-crossing times from the signal. Returns a Vibrapy signal
        with integer base and zero-crossing values.

        The shape of the vibrapy signal which using this algorithm must be NX1.
    
    Parameters
    ---------- 

    threshold : float or string. Variable indicating the threshold value used to determine 
        the zero-crossing time.  Threshold value is taken directly for a float value and 
        is calculated as a percentage of the range if a string is given.  String is converted
        to an float between 0 and 1.  If the value is out of this range the threshold is set 
        to 0.5 times the range.


    lower_lim : float.  Value indicating the lower limit of the signal. Is calculated if not
            given.

    higher_lim : float.  Value indicating the higher limit of the signal. Is calculated if not
            given.

    gradient_positive : bool.  Indicated whether the signal should be triggered on the rise or
         fall of the signal.  Defaults to True.

    min_sep : int.  Minimum seperation between consequtive zero-crossing times.  Defaults to  5.

    dt : float.  Time between consecutive samples.  Defaults to 1.
    
    disp: boolean.  Indicates whether the thresholding parameters should be displayed.

    '''

    def __init__(self, threshhold=None, lower_lim=None, higher_lim=None, gradient_positive=True, min_sep=5, disp=True):
        super().__init__()
        self.threshhold = threshhold
        self.lower_lim = lower_lim
        self.higher_lim = higher_lim
        self.gradient_positive = gradient_positive
        self.min_sep = min_sep
        self.disp = disp
    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):
        if matrix.shape[1] != 1:
            raise ValueError('The shape of the Tacho signal must be NX1. Current shape is {}'.format(matrix.shape))
        signal = matrix[:,0]
        # Set the lower limit if not already set
        if not self.lower_lim:
            self.lower_lim = min(signal)
        # Set the higher limit if not already set
        if not self.higher_lim:
            self.higher_lim = max(signal)

        if types.isNum(self.lower_lim) == False:
            raise ValueError('Lower limit of {} is not a valid number.'.format(
                self.lower_lim))
        if types.isNum(self.higher_lim) == False:
            raise ValueError('Higher limit of {} is not a valid number.'.format(
                self.higher_lim))
        
        dt = base[2] - base[1]

        signal_range = self.higher_lim - self.lower_lim
        # Determine threshold value if not given as in the middle of the range
        if not self.threshhold:
            self.threshhold = '0.5'
        # Determine the threshold value from the argument
        if self.threshhold is None:
            self.threshhold = '0.5'

        if isinstance(self.threshhold, str):
            # If the threshold value is a string, it denotes 
            # a value between 0 and 1 where the threshold
            # is then a percentage of the range of the 
            # solution.
            try:
                self.threshhold = float(self.threshhold)
                if not (self.threshhold <=1 and self.threshhold >=0):
                    warnings.warn('The threshold value is not within the range 0 - 1.  Defaulting to 0.5.')
                    threshhold = 0.5
            except:
                raise ValueError("Cannot convert the threshold value {} to a float between 0 and 1.  Please pass a convertable string".format(self.threshhold))
            self.threshhold = self.lower_lim + signal_range*self.threshhold
        elif types.isNum(self.threshhold):
            pass
        else:
            raise ValueError("Cannot interpret the threshold value {} as a valid threshold.".format(self.threshhold))
        if self.threshhold < self.lower_lim or self.threshhold > self.higher_lim:
            raise ValueError("The threshhold value of {} is smaller than the lower limit of {} or higher than the higher limit of {}.".format(self.threshhold,
                                                                                                                                              self.lower_lim,
                                                                                                                                              self.higher_lim))
        if not isinstance(self.gradient_positive, bool):
            raise ValueError("gradient_positive value of {} is not a valid boolean value.".format(self.gradient_positive))
        if self.gradient_positive:
            gradient_value = 2
        else:
            gradient_value = -2
        if self.disp:
            print('Extract parameters ----------')
            print('Lower limit:', self.lower_lim)
            print('Higher limit:', self.higher_lim)
            print('Signal Range:', signal_range)
            print('Threshold:', self.threshhold)
            print('-----------------------------')
        # Determine the indices of the first values after the zero crossing
        signed_signal_diff = diff(sign(array(signal) - self.threshhold))
        indices_diff_cross = arange(1, len(signal))[signed_signal_diff == gradient_value]
        # Perform interpolation
        zero_crossings = indices_diff_cross -\
                         (signal[indices_diff_cross] - self.threshhold )\
                        /(signal[indices_diff_cross] - signal[indices_diff_cross-1])
        # Remove zero-crossings that are closer than min_sep to the previous one
        no = 0
        while True:
            no += 1
            if no == 10:
                warnings.warn('This is the tenth time that double zero-crossings are removed.  This indicates that a threshhold value has not been set properly.  Please check that the identified zero-crossing times are correct as I am now stopping the removal of double triggers.')
                break
            space              = insert(ediff1d(zero_crossings), 0, self.min_sep + 1)
            signed_space_diff  = diff(sign(space - self.min_sep))
            if sum(signed_space_diff == 0) == len(signed_space_diff):
                break
            keep_list          = insert(1 + arange(len(signed_space_diff))[signed_space_diff != -2], 0, 0).astype(int)
            zero_crossings = zero_crossings[keep_list]
        # Operation complete, return zero-crossing times
        signal_zero_crossing = np.atleast_2d(zero_crossings * dt).T
        base = np.arange(len(zero_crossings))
        return signal_zero_crossing, base, signalNames, 'Zero-crossing time', base_unit, 'Ordinal', 'Number'

class comp(algorithm):
    
    ''' This algorithm performs Baysian Geometry compensation on a vibrapy signal of consequtive 
        zero-crossing times. The signal is truncated to N*M+1 values where M is the amount of revolutions
        to perform the algorithm on.

        If using this algorithm, please cite:

        Diamond, D.H., Heyns, P.S. and Oberholster, A.J., 2016. Online shaft encoder geometry compensation
        for arbitrary shaft speed profiles using Bayesian regression. Mechanical Systems and
        Signal Processing, 81, pp.402-418.

        Signal must be shape (NX1).
    
    Parameters
    ---------- 

    N:  The number of sections in the shaft encoder.

    M:  The number of complete revolutions over which the compensation must be performed.

    e:  An initial estimate for the encoder geometry.  If left an empty array, all sections are assumed equal.

    beta:   Precision of the likelihood function.

    sigma:  Standard deviation of the prior probability.     

    '''

    def __init__(self, N, M=20, e=[], beta=10.0e10, sigma=10.0):
        super().__init__()
        self.N = N
        self.M = M
        self.e = e
        self.beta = beta
        self.sigma = sigma
    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):
        if matrix.shape[1] != 1:
            raise ValueError('The signal must be shape NX1. Current shape is {}'.format(matrix.shape))
        t = matrix[:self.N*self.M+1,0]
        if len(t) != self.M*self.N + 1:
            raise ValueError('There is only {} values in this signal, there must be M*N+1 at least {} values'.format(len(t), self.N*self.M+1 ))
        if len(self.e) != 0 and len(self.e) != self.N:
            raise ValueError('The encoder input should either be an empty list or a list with N elements')
        # Initialize matrices
        A = np.zeros((2*self.M*self.N - 1,self.N + 2*self.M*self.N))
        B = np.zeros((2*self.M*self.N - 1,1))
        # Calculate zero-crossing periods
        T = np.ediff1d(t)
        # Insert Equation (11)
        A[0,:self.N] = np.ones(self.N)
        B[0,0] = 2*np.pi
        # Insert Equation (9) into A
        deduct = 0
        for m in range(self.M):
            if m==self.M-1:
                deduct = 1
            for n in range(self.N-deduct):
                nm = m*self.N + n
                A[1 + nm,n] =                 3.0
                A[1 + nm,self.N + nm*2] =         -1.0/2 * T[nm]**2
                A[1 + nm,self.N + nm*2 + 1] =     -2 * T[nm]
                A[1 + nm,self.N + (nm+1)*2 + 1] = -1 * T[nm]
        # Insert Equation (10) into A
        deduct = 0
        for m in range(self.M):
            if m==self.M-1:
                deduct = 1
            for n in range(self.N-deduct):
                nm = m*self.N + n
                A[self.M*self.N + nm,n] =                 6.0
                A[self.M*self.N + nm,self.N + nm*2] =         -2 * T[nm]**2
                A[self.M*self.N + nm,self.N + (nm+1)*2] =     -1 * T[nm]**2
                A[self.M*self.N + nm,self.N + nm*2 + 1] =     -6 * T[nm]
        # Initialize prior vector
        m0 = np.zeros((self.N + 2*self.M*self.N,1))
        # Initialize and populate covariance matrix of prior
        Sigma0 = np.identity(self.N + 2*self.M*self.N) * self.sigma**2
        # Populate prior vector
        if len(self.e) == 0:
            eprior = np.ones(self.N) * 2*np.pi/self.N
        else:
            eprior = np.array(self.e) * 1.0
        m0[:self.N,0] = eprior * 1.0
        for m in range(self.M):
            for n in range(self.N):
                nm = m*self.N + n
                m0[self.N + nm*2 + 1,0] = m0[n,0] / T[nm]
        # Solve for mN (or x)
        SigmaN = Sigma0 + self.beta * A.T.dot(A)
        BBayes = Sigma0.dot(m0) + self.beta * A.T.dot(B)

        mN = np.array([spsolve(csr_matrix(SigmaN),csr_matrix(BBayes))]).T
        
        # Normalize encoder increments to add up to 2 pi
        epost = np.atleast_2d(mN[:self.N,0] * 2 * np.pi / (np.sum(mN[:self.N,0]))).T
        
        # Return encoder geometry
        return epost, np.arange(1, self.N+1), ['{} Geometry'.format(signalNames[0])], ['Distance'], ['rad'], 'Section number', '.'

class StatefromMPR(algorithm):
    '''
    Function to calculate the IAS State variable values for an MPR shaft encoder zero-crossing time
    signal. Signal must be NX1.
    
    Parameters
    ----------

    M : The amount

    '''
    def __init__(self, geom, fs=102.4e3, return_all=False):
        super().__init__()
        if types.isVibrapy(geom):
            self.geom = geom.values().flatten()
        else:
            self.geom = geom
        self.fs = fs
        self.return_all = return_all
    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):
        
        def kryMats(m,T,mu_min1,V_min1):
            C = array([[0.5*T[m],0.5*T[m]]])
            if m > 0:
                #Vir alle toestande na 1
                A = array([[0,1], [-T[m]/T[m-1],1 + T[m]/T[m-1]]])
                Gamma = (   0.5 *  0.01 * A.dot(mu_min1) * identity(2)    )  **2
                P_min1 = A.dot(V_min1).dot(A.T) + Gamma
                Sigma = array((0.25 * 1/self.fs * mu_min1.T.dot(A.T.dot(array([[1,1]]).T))))**2
                K = P_min1.dot(C.T) * inv((C.dot(P_min1.dot(C.T)) + Sigma))
            else:
                #Vir eerste toestand
                A = array([[1,0], [0,1]])
                Gamma = (0.5 * 0.01 * A.dot(mu_min1) * identity(2))**2
                P_min1 = A.dot(V_min1).dot(A.T) + Gamma
                Sigma = array((0.25 * 1/self.fs * mu_min1.T.dot(A.T.dot(array([[1,1]]).T))))**2
                K = P_min1.dot(C.T) * inv((C.dot(P_min1.dot(C.T)) + Sigma))
            return A,C,P_min1,Gamma,Sigma,K

        # Calculate the amount of complete
        # shaft revolutions in the shaft
        if matrix.shape[1] != 1:
            raise ValueError('The signal must be shape NX1. Current shape is {}'.format(matrix.shape))
        MPR = matrix[:, 0]
        S = len(self.geom)
        
        M = int(np.floor( (len(MPR)-1)/S ))
        T = ediff1d(MPR)[:M*S]

        mu=[array([[self.geom[0]/T[0], self.geom[1]/T[1] ]]).T]
        V =[ (0.005 * mu[0]* identity(2))**2]
        x = []
        for s in range(S):
            x.append(array([[self.geom[s]]]))

        for m in range(M):
            for s in range(S):
                A,C,P_min1,Gamma,Sigma,K = kryMats(m*S+s,T,mu[-1],V[-1])
                mu.append(A.dot(mu[-1]) + K.dot(x[s] - C.dot(A.dot(mu[-1]))))
                V.append((identity(2) - K.dot(C)).dot(P_min1))

        mu, V = array(mu)[1:,:,:], array(V)[1:,:,:]

        matrix_back      = np.zeros((mu.shape[0], 7))
        # Assign the state variable mean values
        matrix_back[:,0] = mu[:,0,0]
        matrix_back[:,1] = mu[:,1,0]
        # Assign the state variable variances
        matrix_back[:,2] = V[:,0,0]
        matrix_back[:,3] = V[:,0,1]
        matrix_back[:,4] = V[:,1,1]
        # Assign the section period lengths
        matrix_back[:,5] = T
        # Assign the start angle to each state 
        geom_start        = list(np.cumsum([0] + list(self.geom[:-1])))
        geom_amount       = int(np.ceil(matrix_back.shape[0]/len(geom_start)))
        geom_start_list   = geom_start * geom_amount
        matrix_back[:,6]  = geom_start_list[:matrix_back.shape[0]]

        return matrix_back, MPR[:matrix_back.shape[0]],\
               [r'$\mu_{z,0}$', r'$\mu_{z,1}$', r'$V_{00}$', r'$V_{01}$', r'$V_{11}$', r'$T$', r'$\theta_0$'],\
                 ['Angular velocity', 'Angular velocity', 'Variance', 'Variance', 'Variance', 'Period', 'Angle'],\
                 [r'$\frac{rad}{' + signal_unit[0] + r'}$',
                r'$\frac{rad}{' + signal_unit[0] + r'}$',
                r'$(\frac{rad}{'+ signal_unit[0] + r'})^2$',
                r'$(\frac{rad}{'+ signal_unit[0] + r'})^2$',
                r'$(\frac{rad}{'+ signal_unit[0] + r'})^2$',
                r's',
                r'$rad$'],\
                 'Temporal',\
                 's'

class StatefromOPR(algorithm):
    '''
    Function to calculate the IAS State variable values for an OPR shaft encoder zero-crossing time
    signal. Signal must be NX1.
    
    Parameters
    ----------

    M : The amount

    '''
    def __init__(self, fs=102.4e3):
        super().__init__()
        self.fs = fs

    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):
        
        def kryMats(m,T,mu_min1,V_min1):
            C = array([[0.5*T[m],0.5*T[m]]])
            if m > 0:
                #Vir alle toestande na 1
                A      = array([[0,1], [-T[m]/T[m-1],1 + T[m]/T[m-1]]])
                Gamma  = (0.5 * 0.01 * A.dot(mu_min1) * identity(2))  **2
                P_min1 = A.dot(V_min1).dot(A.T) + Gamma
                Sigma  = array((0.25 * 1/self.fs * mu_min1.T.dot(A.T.dot(array([[1,1]]).T))))**2
                K      = P_min1.dot(C.T) * inv((C.dot(P_min1.dot(C.T)) + Sigma))
            else:
                #Vir eerste toestand
                A = array([[1,0], [0,1]])
                Gamma = (0.5 * 0.01 * A.dot(mu_min1) * identity(2))**2
                P_min1 = A.dot(V_min1).dot(A.T) + Gamma
                Sigma = array((0.25 * 1/self.fs * mu_min1.T.dot(A.T.dot(array([[1,1]]).T))))**2
                K = P_min1.dot(C.T) * inv((C.dot(P_min1.dot(C.T)) + Sigma))
            return A,C,P_min1,Gamma,Sigma,K

        if matrix.shape[1] != 1:
            raise ValueError('The signal must be shape NX1. Current shape is {}'.format(matrix.shape))
        OPR = matrix[:, 0]

        T   = ediff1d(OPR)
        M   = len(T)
        
        mu  =[array([[2*pi/T[0], 2*pi/T[1] ]]).T]
        V   =[ (0.005 * mu[0]* identity(2))**2]
        x   = array([[2*pi]])

        for m in range(M):
            A,C,P_min1,Gamma,Sigma,K = kryMats(m,T,mu[-1],V[-1])
            mu.append(A.dot(mu[-1]) + K.dot( x - C.dot(A.dot(mu[-1])))  )
            V.append((identity(2) - K.dot(C)).dot(P_min1) )
        
        mu,V  = array(mu)[1:,:,:], array(V)[1:,:,:]
        
        a_mu  = 1/T*mu[:,1,0] - 1/T * mu[:,0,0]
        a_var = (1/T)**2 * V[:,1,1] - (1/T)**2 * V[:,0,0] - 2/T**2 * V[:,0,1]


        matrix_back      = np.zeros((mu.shape[0], 7))
        # Assign the state variable mean values
        matrix_back[:,0] = mu[:,0,0]
        matrix_back[:,1] = mu[:,1,0]
        # Assign the state variable variances
        matrix_back[:,2] = V[:,0,0]
        matrix_back[:,3] = V[:,0,1]
        matrix_back[:,4] = V[:,1,1]
        # Assign the section period lengths
        matrix_back[:,5] = T
        
        return matrix_back, OPR[:matrix_back.shape[0]],\
               [r'$\mu_{z,0}$', r'$\mu_{z,1}$', r'$V_{00}$', r'$V_{01}$', r'$V_{11}$', r'$T$', r'$\theta_0$'],\
                 ['Angular velocity', 'Angular velocity', 'Variance', 'Variance', 'Variance', 'Period', 'Angle'],\
                 [r'$\frac{rad}{' + signal_unit[0] + r'}$',
                r'$\frac{rad}{' + signal_unit[0] + r'}$',
                r'$\left(\frac{rad}{'+ signal_unit[0] + r'}\right)^2$',
                r'$\left(\frac{rad}{'+ signal_unit[0] + r'}\right)^2$',
                r'$\left(\frac{rad}{'+ signal_unit[0] + r'}\right)^2$',
                r's',
                r'$rad$'],\
                 'Temporal',\
                 's'

class IAPfromState(algorithm):

    '''
    Algorithm to calculate the IAP for a specific t values according to Eq. 48

    Parameters
    ----------
    t: The global time values used for the analysis.

    geom : The encoder geometry.

    '''
    def __init__(self, t, return_probs=False):
        if __types__.isVibrapy(t):
            self.t = t.values().flatten()
        else:
            self.t = np.array(t).flatten()

        self.return_probs = return_probs
    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):

        values_short = set([r'$\mu_{z,0}$', r'$\mu_{z,1}$', r'$V_{00}$', r'$V_{01}$', r'$V_{11}$', r'$T$', r'$\theta_0$']) - set(signalNames)
        if len(values_short) != 0:
            raise ValueError(  'The following signals are missing: {}'.format( ', '.join(list(values_short) )))
        
        mu_z0       = matrix[:, signalNames.index(r'$\mu_{z,0}$')]
        mu_z1       = matrix[:, signalNames.index(r'$\mu_{z,1}$')]
        V_00        = matrix[:, signalNames.index(r'$V_{00}$')]
        V_01        = matrix[:, signalNames.index(r'$V_{01}$')]
        V_11        = matrix[:, signalNames.index(r'$V_{11}$')]
        T           = matrix[:, signalNames.index(r'$T$')]
        theta_0     = matrix[:, signalNames.index(r'$\theta_0$')]

        t                            = np.atleast_2d(self.t).T
        MPR_incs                     = np.atleast_2d(base).T
        
        taus_MPR_array               = t.dot(np.ones_like(MPR_incs).T)
        diff_array                   = MPR_incs.flatten() - taus_MPR_array
        diff_array[diff_array > 0]   = diff_array.max()
        diff_array = abs(diff_array)
        diff_array_sections          = np.argmin(diff_array, axis=1)
        tau                          = np.min(diff_array, axis=1)

        tau_term1 = (tau - tau**2/(2*T[diff_array_sections]))
        tau_term2 = (tau**2/(2*T[diff_array_sections]))

        mu_tau      = tau_term1 * mu_z0[diff_array_sections] +\
                      tau_term2 * mu_z1[diff_array_sections] +\
                      theta_0[diff_array_sections]
        if self.return_probs == True:
            sigma_2_tau = tau_term1**2 * V_00[diff_array_sections] +\
                          tau_term2**2 * V_11[diff_array_sections] +\
                          2 * tau_term1 * tau_term2 * V_01[diff_array_sections]

            matrix_back      = np.zeros((mu_tau.shape[0], 2))
            
            matrix_back[:,0] = mu_tau
            matrix_back[:,1] = sigma_2_tau

            return matrix_back, self.t,\
                   [r'$\mu_{\theta}}$', r'$\sigma_{\theta}^2$'],\
                     ['IAP Mean', 'IAP Variance'],\
                     [r'rad', r'rad$^2$'],\
                     'Temporal',\
                     's'
        else:
            matrix_back      = np.zeros((mu_tau.shape[0], 1))
            matrix_back[:,0] = mu_tau
            return matrix_back, self.t,\
                   [r'$\theta$'],\
                     ['IAP'],\
                     [r'rad'],\
                     'Temporal',\
                     's'

class constantStateFromOPR(algorithm):
    
    '''
    Algorithm to calculate a constant speed state variable similar to
    StateFromOPR.
    '''
    def __init__(self):
        pass

    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):

        if matrix.shape[1] != 1:
            raise ValueError('The signal must be shape NX1. Current shape is {}'.format(matrix.shape))

        OPR         = matrix[:,0]
        OPR_diff    = np.diff(OPR)
        Shaft_speed = 2*np.pi/(OPR_diff)

        matrix_back       = np.zeros((len(Shaft_speed), 2))
        matrix_back[:, 0] = Shaft_speed

        return matrix_back, OPR[:len(Shaft_speed)],\
               [r'$\Omega$', r'$\theta_0$'],\
                 ['Angular speed','Starting position'],\
                 [r'$\frac{rad}{'+signal_unit[0]+'}$', 'rad'],\
                 'Temporal',\
                 's'

class constantStateFromMPR(algorithm):
    
    '''
    Algorithm to calculate a constant speed state variable similar to
    StateFromOPR.
    '''
    def __init__(self, geom):
        super().__init__()
        if types.isVibrapy(geom):
            self.geom = geom.values().flatten()
        else:
            self.geom = geom

    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):

        if matrix.shape[1] != 1:
            raise ValueError('The signal must be shape NX1. Current shape is {}'.format(matrix.shape))

        MPR           = matrix[:,0]
        MPR_diff      = np.diff(MPR)
        
        M             = int(np.ceil(len(MPR)/len(self.geom)) + 1)
        geom_list     = np.array(list(self.geom) * M)
        geom_list_cum = np.array(list(np.cumsum(([0] + list(self.geom[:-1]))))*M)
        
        Shaft_speed   = np.array(geom_list)[:MPR_diff.shape[0]]/MPR_diff

        matrix_back       = np.zeros((len(Shaft_speed), 2))
        matrix_back[:, 0] = Shaft_speed
        matrix_back[:, 1] = geom_list_cum[:len(Shaft_speed)]

        return matrix_back, MPR[:len(Shaft_speed)],\
               [r'$\Omega$', r'$\theta_0$'],\
                 ['Angular speed','Starting position'],\
                 [r'$\frac{rad}{'+signal_unit[0]+'}$', 'rad'],\
                 'Temporal',\
                 's'

class IAPFromConstantState(algorithm):
    
    ''' A function used to calculate the shaft IAP
    assuming a constantState variable.
    '''

    def __init__(self, t):
        if __types__.isVibrapy(t):
            self.t = t.values().flatten()
        else:
            self.t = np.array(t).flatten()

    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):
        values_short = set([r'$\Omega$', r'$\theta_0$']) - set(signalNames)

        if len(values_short) != 0:
            raise ValueError(  'The following signals are missing: {}'.format( ', '.join(list(values_short) ))  )
        
        omega       = matrix[:, signalNames.index(r'$\Omega$')]
        theta_0     = matrix[:, signalNames.index(r'$\theta_0$')]

        t                            = np.atleast_2d(self.t).T
        MPR_incs                     = np.atleast_2d(base).T
        
        taus_MPR_array               = t.dot(np.ones_like(MPR_incs).T)
        diff_array                   = MPR_incs.flatten() - taus_MPR_array
        diff_array[diff_array > 0]   = diff_array.max()
        diff_array = abs(diff_array)
        diff_array_sections          = np.argmin(diff_array, axis=1)
        tau                          = np.min(diff_array, axis=1)

        iap                          = omega[diff_array_sections] * tau + theta_0[diff_array_sections]

        matrix_back      = np.zeros((iap.shape[0], 1))
        matrix_back[:,0] = iap

        return matrix_back, self.t,\
               [r'$\theta$'],\
               ['Angle'],\
               [r'rad'],\
               'Temporal',\
               's'