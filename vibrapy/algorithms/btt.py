'''
Module containing signal processing techniques for BTT functions in Vibrapy.
==========================================================
Authors: David Hercules Diamond

License: MIT

All functions must take arguments and return certain arguments:

Parameters
----------
matrix : NDArray.  A matrix containing signals in the temporal domain.  Each column must be a different signal.

base : NDArray.  The temporal base of the signal.

signalNames : List containing the signal name for each signal in matrix.

signalType : List containing the types for the signals in matrix.

signal_unit : List containing the units for each signal in signalNames.

base_type : string.  A function specifying the base type.  If base_type
is not "Temporal" the function simply returns the inputs along with a 
warning.

base_unit: string.  Function value giving the base_unit.

signal : Signal or angularSignal.  The signal instance from which this
function was called. 

args : dict containing additional arguments.

Return
------
Transformed values for the following variables:

matrix

base

signalNames 

signalType 

signal_unit 

base_type 

base_unit 
'''
import warnings

import numpy as __np__
from scipy.interpolate import interp1d as __interp1d__
from vibrapy.utils import types as __types__
import copy as __copy__
from vibrapy.algorithms.general import algorithm
from matplotlib import pyplot as __plt__

class bttToAConstantCrossing(algorithm):
    '''
    Function to calculate ToAs from a BTT proximity sensor signal using the 
    constant fraction crossing method.

    Parameters
    ----------

    constant_fraction : The constant fraction of the range by which the signal
        must drop to trigger a ToA

    range_percentage : 

    min_sep: The minimum number of samples that must separate consecutive ToAs.
    '''
    def __init__(self, constant_fraction=None, range_percentage=None, min_sep=None):
        super().__init__()
        self.constant_fraction = constant_fraction
        self.range_percentage = range_percentage
        self.min_sep = min_sep
    def process(self, matrix, base, signalNames, signalType, signal_unit, base_type, base_unit, signal):
        if __types__.isNotNone(self.constant_fraction):
            constant_fraction = self.constant_fraction
        else:
            constant_fraction=0.5

        if __types__.isNotNone(self.range_percentage):
            range_percentage = self.range_percentage
        else:
            range_percentage = 0.1

        if __types__.isNotNone(self.min_sep):
            min_sep = self.min_sep
        else:
            min_sep = 3

        if constant_fraction >= 1 or constant_fraction <=0:
            raise ValueError('Constant fraction must be a number between 0 and 1')
        if range_percentage >= 1 or range_percentage <=0:
            raise ValueError('Range percentage must be a number between 0 and 1')
        if min_sep < 0:
            raise ValueError('min_sep must be smaller larger than 0')
        if base_type == 'Temporal':
            ToAs_signal = []
            for signal_nr in range(matrix.shape[1]):
                # Compress signal to between 0 and 1
                temp_signal = (matrix[:,signal_nr] - matrix[:,signal_nr].min()) / (matrix[:,signal_nr].max() - matrix[:,signal_nr].min())
                temp_signal[temp_signal < __np__.median(temp_signal)] = __np__.median(temp_signal)
                # Get available range
                minimum_value   = temp_signal.min()
                available_range = 1 - temp_signal.min()
                cutoff          = available_range * range_percentage + temp_signal.min()
                # Get the cutoff left values
                diff_signal           = __np__.diff(__np__.sign(temp_signal - cutoff))
                left_boundary         = __np__.nonzero(diff_signal>0)[0]
                right_boundary        = __np__.nonzero(diff_signal<0)[0]
                ediff_left_boundary   = __np__.ediff1d(left_boundary)
                ediff_right_boundary  = __np__.ediff1d(left_boundary)

                left_boundary         = left_boundary[__np__.array([True]  +  list(ediff_left_boundary >= min_sep), dtype=bool)]
                right_boundary        = right_boundary[__np__.array([True] + list(ediff_right_boundary >= min_sep), dtype=bool)]
                toas                  = []
                crossing              = []
                length_left           = len(left_boundary)
                for i in range(len(left_boundary)):
                    print('Processing ToA {} of {}. Progress: {}%. Approximate time: {}'.format(i+1,length_left, round((i+1)/length_left*100,2), base[left_boundary[i]]))
                    
                    # Find closest right_boundary after left_boundary[i]
                    left_value            = left_boundary[i] 
                    try:
                        right_value       = right_boundary[__np__.nonzero(right_boundary > left_value)[0][0]]
                    except:
                        print('No right value found for left value {}'.format(i))
                        continue
                    maximum_value   = temp_signal[left_value:right_value+1].max()
                    cutoff_value    = (maximum_value - minimum_value)*constant_fraction + minimum_value
                    try:
                        downwards_value = left_value + __np__.nonzero(__np__.diff(__np__.sign(temp_signal[left_value:right_value+1] - cutoff_value)) < 0)[0][-1]
                        index_xstar     = downwards_value + (temp_signal[downwards_value] - cutoff_value)/(temp_signal[downwards_value] - temp_signal[downwards_value+1])
                        toas.append(index_xstar*1)
                        crossing.append(cutoff_value)
                    except:
                        pass
                ToAs_signal.append(__interp1d__(__np__.arange(len(base)), base)(toas))
            lowest_toa = min([len(i) for i in ToAs_signal])
            matrix = __np__.zeros((lowest_toa,matrix.shape[1]))
            for i, j in enumerate(ToAs_signal):
                matrix[:, i] = j[:lowest_toa]
            base        = __np__.arange(lowest_toa)
            signalNames = ['ToA of {}'.format(i) for i in signalNames]
            signalType  = ['Temporal' for i in signalNames]
            signal_unit = [base_unit for i in signalNames]
            base_type   = 'Temporal'
            base_unit   = 'Count'
        else:
            __warnings__.warn('Input is not temporal.  Returning input parameters'
            ' unchanged.')
        return matrix, base, signalNames, signalType, signal_unit, base_type, base_unit

class disparityFilter(algorithm):
    '''
    Function used to calculate the disparity filter values for a vibrapy BTT signal.
    The vibrapy signal must be NXM where N is the number of revolutions and M is the number of sensors.
    '''
    def __init__(self):
        super().__init__()
    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):

        matrix_new = __np__.atleast_2d(matrix.max(axis=1) - matrix.min(axis=1)).T
        return matrix_new, base, ['Disparity'], ['Displacement'], [signal_unit[0]], base_type, base_unit

class uConditionNumber(algorithm):
    '''
    Function used to calculate the unimodal condition numbers for a set of AoA values and 
    a set of EO values.
    '''
    def __init__(self, EOs=None, EOmin=None, EOmax=None, offset=False):
        super().__init__()
        self.EOs    = EOs
        self.EOmin  = EOmin
        self.EOmax  = EOmax
        self.offset = offset
    def process(self, matrix, base, signalNames, signalType,\
            signal_unit, base_type, base_unit,\
            signal):
        if __types__.isNotNone(self.EOmin) and __types__.isNotNone(self.EOmax):
            EOs = __np__.arange(self.EOmin, self.EOmax+1)
        elif __types__.isNotNone(self.EOs) and __types__.isIter(self.EOs):
            EOs = self.EOs
        else:
            raise ValueError('You must specify EOs as an iterable or both EOmin and EOmax as integers')

        matrix_new = __np__.zeros((matrix.shape[0], len(EOs) + 1))
        cutoff = 2 if self.offset == False else 3
        for i, EO in enumerate(EOs):
            for j, sensors in enumerate(matrix):
                Phi = __np__.array([ __np__.sin(EO * sensors),
                                     __np__.cos(EO * sensors),
                                     __np__.ones_like(sensors)]).T[:, :cutoff]
                matrix_new[j, i] = __np__.linalg.cond(Phi)
        return matrix_new,  base, ['\kappa({})'.format(i) for i in EOs] + ['\sum \kappa(EOs)'] , 'Condition number', '', base_type, base_unit