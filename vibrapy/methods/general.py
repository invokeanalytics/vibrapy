'''
Module containing general signal processing methods for Vibrapy.
===============================================
Authors: David Hercules Diamond

License: MIT
'''

class __method__:
    def __init__(self):
        pass