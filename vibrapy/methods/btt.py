'''
Module for containing Methods to work with Instantaneous Angular Speed (IAS) from zero
crossing times and the encoder geometry. 
===============================================
Authors: David Hercules Diamond

License: MIT
'''
from vibrapy.methods.general import __method__
from vibrapy.utils import types as __types__
import numpy as __np__
import vibrapy as __vp__
from copy import copy, deepcopy
import datetime

from matplotlib import pyplot as plt
from pyswarm import pso
from pandas import Series

import numpy as np
class spacingEstimator(__method__):
    ''' A class that is used to estimate the BTT spacing using the method 
    described in DOI 10.1115/1.4039931.
    

    Parameters
    ----------

    sensor_amount : The number of sensors in the system.

    first_theta_val : The fixed value of the first sensor. Defaults to None if 
        the first sensor's position can vary.

    minimum_sep : integer. The minimum allowable distance between consequtive sensors.

    weights : A dictionary containing key-value pairs where the keys are specific
        EOs and the values are the corresponding weights. Defaults to an empty list.

    fixtures : List of two-value lists. Each two-value list in fixtures
        represents the start and end of a fixture where the sensors cannot
        be.

    EOmin : Integer. The minimum EO to take into account.

    EOmax : Integer. The maximum EO to take into account.

    EOs : A list of EOs to take into account. If specified, EOmin
        and EOmax is not used
    '''
    def __init__(self, sensor_amount, first_theta_val=None, minimum_sep=0, weights={}, fixtures=[], EOmin=1, EOmax=100, EOs=None):
        self.sensor_amount = copy(sensor_amount)
        self.first_theta_val = copy(first_theta_val)
        self.minimum_sep = copy(minimum_sep)
        self.weights = weights
        self.fixtures = fixtures
        self.EOmin = copy(EOmin)
        self.EOmax = copy(EOmax)
        self.EOs = copy(EOs)

        self.solutions = []
    def clearSolutions(self):
        ''' A function that clears all solutions and returns the previous slate.
        '''
        solutions = deepcopy(self.solutions)
        self.solutions = []
        return solutions
    def calcCondition(self, thetas, EO):
        design = __np__.array([__np__.sin(EO*thetas), __np__.cos(EO*thetas), __np__.ones_like(thetas)]).T
        cond = __np__.linalg.cond(design)
        return cond
    def getEOs(self):
        ''' A function that is used to obtain a list of EOs from
        the self.EOmin, self.EOmax and self.EOs parameters.

        Returs the list 
        '''
        if __types__.isList(self.EOs):
            if not __types__.isEmptyList(self.EOs):
                EOs = copy(self.EOs)
            else:
                __types__.isEmptyList(self.EOs, 'EOs cannot be an empty list')
        else:
            if not (__types__.isNatural(self.EOmin) and  __types__.isNatural(self.EOmax) ):
                raise ValueError('Both EOmin and EOmax must be larger than 1 and an integer if EOs is not specified.')
            else:
                EOs = __np__.arange(self.EOmin, self.EOmax+1)
        return EOs
    def spacingEvalFunc(self, thetas, EOs, weights, minimum_sep, fixtures, first_theta_val = None):
        ''' A function that calculates the sum of squared weigted condition
        numbers for a BTT system and a particular rotor.
        
        Parameters
        ----------
        thetas : Array. The parameter vector for the PSO method. Consists of
            the first sensor position in the first position of the array 
            and each corresponding sensor increment for the other values.
            Values in radians.

        weights : The weighting values for the cost function.

        minimum_sep : The minimum distance that must separate two sensors.
            Minimum distance in radians.
        
        fixtures : List of two-value lists. Each two-value list in fixtures
            represents the start and end of a fixture where the sensors cannot
            be.


        first_theta_val : The value of the first sensor. If true, each value
        of thetas corresponds to the next sensor's spacing.

        Returns
        -------
        1 if requirement is met else -1
        '''
        if __types__.isNum(first_theta_val):
            thetas_surr = __np__.append(first_theta_val, thetas)            
        else:
            thetas_surr = copy(thetas)
        
        thetas_cum = thetas_surr.cumsum()
        weighted_sum_of_squares = __np__.sum([(weights[i] * self.calcCondition(thetas_cum, EOs[i]))**2 for i in range(len(EOs))])
        return weighted_sum_of_squares
    def runPSO(self, debug=False, swarmsize=5000, maxiter=30):
        ''' A function used to run the PSO algorithm given the metadata
            at hand.

            Parameters
            ----------

            debug : Boolean. Wheter to print out debug messages.

            swarmsize : The size of the PSO swarm.

            maxiter : The maximum amount of function iterations.
        '''
        def mindistFunc(thetas, EOs, weights, minimum_sep, fixtures, first_theta_val = None):
            ''' A function that determines if there are any 
            sensor spacing values that are within a minimum_sep from
            one another.

            Parameters
            ----------
            thetas : Array. The parameter vector for the PSO method. Consists of
                the first sensor position in the first position of the array 
                and each corresponding sensor increment for the other values.
                Values in radians.

            weights : The weighting values for the cost function.

            minimum_sep : The minimum distance that must separate two sensors.
                Minimum distance in radians.
            
            fixtures : List of two-value lists. Each two-value list in fixtures
                represents the start and end of a fixture where the sensors cannot
                be.


            first_theta_val : The value of the first sensor. If true, each value
            of thetas corresponds to the next sensor's spacing.

            Returns
            -------
            1 if requirement is met else -1
            '''
            if __types__.isNum(first_theta_val):
                thetas_surr = __np__.append(first_theta_val, thetas)            
            else:
                thetas_surr = copy(thetas)

            thetas_cum = __np__.array([i % (2*__np__.pi) for i in thetas_surr.cumsum()])
            if __np__.any(abs(__np__.ediff1d(thetas_cum)) <= minimum_sep):
                return -1
            return 1
        def fixturesFunc(thetas, EOs, weights, minimum_sep, fixtures, first_theta_val = None):
            ''' A function used to determine if any of the sensors lie
            within a designated fixture zone.

            Parameters
            ----------
            thetas : Array. The parameter vector for the PSO method. Consists of
                the first sensor position in the first position of the array 
                and each corresponding sensor increment for the other values.
                Values in radians.

            weights : The weighting values for the cost function.

            minimum_sep : The minimum distance that must separate two sensors.
                Minimum distance in radians.
            
            fixtures : List of two-value lists. Each two-value list in fixtures
                represents the start and end of a fixture where the sensors cannot
                be.


            first_theta_val : The value of the first sensor. If true, each value
            of thetas corresponds to the next sensor's spacing.

            Returns
            -------
            1 if requirement is met else -1
            '''

            if __types__.isNum(first_theta_val):
                thetas_surr = __np__.append(first_theta_val, thetas)            
            else:
                thetas_surr = copy(thetas)

            thetas_cum = thetas_surr.cumsum()
            for f in fixtures:
                for t in thetas_cum:
                    if (t % (2*np.pi)) >= f[0] and (t % (2*np.pi)) <= f[1]:
                        return -1
            return 1

        # Determine the EOs list
        EOs = self.getEOs()
        # Get the EOs weights
        weights = []
        weight_keys = self.weights.keys()
        for EO in EOs:
            if EO in weight_keys:
                weights.append(1*self.weights[EO])
            else:
                weights.append(1)

        if __types__.isNotNone(self.first_theta_val): # If the first theta value is specified.
            # Get the minimum distance of each increment.
            minimum_distance = [self.minimum_sep for i in range(self.sensor_amount-1)]
            # Get the maximum distance of each increment
            maximum_distance = [0.5*__np__.pi for i in range(self.sensor_amount-1)]
        else:
            # Get the minimum distance of each increment.
            minimum_distance = [0] + [self.minimum_sep for i in range(self.sensor_amount-1)]
            # Get the maximum distance of each increment
            maximum_distance = [2*__np__.pi]  +  [0.5*__np__.pi for i in range(self.sensor_amount-1)]

        solution, value = pso(self.spacingEvalFunc,
            minimum_distance,
            maximum_distance,
            args=([EOs, weights, self.minimum_sep, self.fixtures]),
            swarmsize=swarmsize,
            maxiter = maxiter,
            ieqcons = [mindistFunc, fixturesFunc],
            debug=debug)
        
        if __types__.isNotNone(self.first_theta_val): # If the first theta value is specified.
            theta_param = [self.first_theta_val] + list(solution)
            thetas = __np__.cumsum(theta_param)
        else:
            thetas = __np__.cumsum(solution)

        self.solutions.append([thetas, value, datetime.datetime.utcnow()])
        return solution, value
    def findSolution(self, solution):
        ''' A function used to obtain a certain solution from
        the self.solutions list.

        Parameters
        ----------

        solution : The sensor spacing. This variable has many meanings.

            Int: An integer refers to a specific solutions in the self.solutions
            list. Obtains the solution from there.

            list: A list means the sensor spacings are passed
                explicitly.

            'max': Use the solution with the largest objective value
                function.

            'min' : use the function with the lowest objective function 
                value.
        '''
        if __types__.isInt(solution):
            thetas, value = self.solutions[solution][0],self.solutions[solution][1]
        elif  __types__.isList(solution):
            thetas = np.array(thetas)
            value = self.spacingEvalFunc(np.ediff1d(thetas), EOs, __np.__.ones_like(EOs), first_theta_val = thetas[0])
        elif __types__.isStr(solution):
            if solution == "min":
                ix_min = __np__.argmin([i[1] for i in self.solutions])
                thetas, value = self.solutions[ix_min][0],self.solutions[ix_min][1]
            elif solution == 'max':
                ix_min = __np__.argmax([i[1] for i in self.solutions])
                thetas, value = self.solutions[ix_min][0],self.solutions[ix_min][1]
            else:
                raise ValueError("If you specify a string; it must be 'min' or 'max'")
        else:
            raise ValueError('solution variable is not')
        return thetas, value

    def plotConditionNumbers(self, solution='min'):
        ''' A function that plots the condition numbers
        of the current BTT system according to the current EO values.
        
        Parameters
        ----------

        solution : The solution variable that is passed to self.findSolution.

        '''
        if len(self.solutions) == 0:
            raise ValueError('There must be at least one solution to plot the Condition numbers')
        EOs = self.getEOs()
        thetas, value = self.findSolution(solution)
        cond_numbs = [self.calcCondition(thetas, EO) for EO in EOs]
        Series(cond_numbs, index=EOs, name='Condition numers').plot(kind='bar')
        plt.title('Objective function value : {}'.format(round(value,2)))
    def plotSinusoid(self, solution, EO):
        '''
        Parameters
        ----------

        solution : The solution variable that is passed to self.findSolution.
        
        EO: The EO for which to plot the sinusoid.
        '''
        thetas, value = self.findSolution(solution)

        x_complete = __np__.linspace(0, 2*__np__.pi, 5000)
        y_complete = __np__.sin(x_complete * EO)

        y_thetas = __np__.sin(thetas*EO)

        plt.plot(x_complete, y_complete, color='k')
        plt.plot(thetas, y_thetas, 'o-')
    def getSolutionThetas(self):
        ''' A function used to return the solution thetas as
            a numpy matrix.
        '''
        return __np__.array( [i[0] for i in self.solutions]  )
    def getSolutionValues(self):
        ''' A function used to return the solution values as
            a numpy array.
        '''
        return __np__.array( [i[1] for i in self.solutions]  )
    def getSolutionTimes(self):
        ''' A function used to return the solution times as
            a numpy array.
        '''
        return __np__.array( [i[2] for i in self.solutions]  )

class uBayesianSynchronous(__method__):
    ''' 
    Implements the unimodal Bayesian fit 
    method for a range of for Synchronous vibration.
    '''
    def __init__(self, offset=False, alpha=1/500**2, beta=1/50**2):
        super(__method__, self).__init__()
        self.EOs = []
        self.params = []
        self.residuals = []
        self.offset = offset
        self.alpha = alpha
        self.beta = beta
    def setData(self, X, AoA):
        ''' 
        Method used to the data of the method. 

        Parameters
        ----------

        X : An NxM Vibrapy signal containing tip deflections from N 
            revolutions and M sensors.

        AoA : An NxM Vibrapy signal containing Angle of Arrivals from
        N revolutions and M sensors.
        '''
        self.X = X.values().flatten()
        self.AoA = AoA.values().flatten()
        self.N = X.shape[0]
        self.M = X.shape[1]
        self.NM = self.N * self.M
    def fit(self, EOs=None, EOmin = None, EOmax = None):
        '''
        A function used to fit the unimodal Least Squares Spectral Analysis method.
        
        Parameters
        ----------

        EOs : A list of EOs for which to fit the LS method. Is not used if EOmin and
            EOmax is specified.

        EOmin : The minimum EO to fit. Is used rather than EOs.

        EOmax : The maximum EO to fit. Is used rather than EOs.
        '''

        if __types__.isNotNone(EOmin) and __types__.isNotNone(EOmax):
            EOs = __np__.arange(EOmin, EOmax+1, 1)
        elif __types__.isNone(EOs):
            raise ValueError('You must specify either EOs or both EOmin and EOmax.')
        self.EOs     = EOs
        self.residuals = []
        self.params = []
        
        cutoff = 2 if self.offset == False else 3
        for EO in self.EOs:
            Phi = __np__.array([__np__.sin(EO*self.AoA),
                                __np__.cos(EO*self.AoA),
                                __np__.ones(len(self.AoA))]).T[:, :cutoff]
            SN_inv = self.alpha * __np__.identity(Phi.shape[1]) + self.beta * Phi.T.dot(Phi)
            SN = __np__.linalg.inv(SN_inv)
            mN = self.beta * SN.dot(Phi.T).dot(__np__.array([self.X]).T)
            self.params.append(mN[:,0])
            residual = __np__.sum((Phi.dot(mN).flatten() - self.X)**2) 
            try:
                self.residuals.append(1*residual)
            except:
                self.residuals.append(-1)
        self.residuals = __np__.array(self.residuals)
        self.residuals[self.residuals==-1] = self.residuals.max()
    def freqSignal(self, func=np.exp):
        '''
        A function that returns the residuals as a vibrapy signal.
        '''
        if len(self.EOs) == 0:
            return None
        return __vp__.Signal(func(self.residuals), base=self.EOs, signalNames='EO probability', signalType='Probability', signalUnit='.',base_type='Frequency',base_unit='EO')
    def paramSignal(self):
        '''
        A function that returns the parameters for each EO as a vibrapy signal.
        '''
        if len(self.EOs) == 0:
            return None
        return __vp__.Signal(__np__.array(self.params), base=self.EOs, signalNames='A', signalType='Coefficients', signalUnit='mu m',base_type='Frequency',base_unit='EO')
    def bestFreq(self, n=0):
        '''
        A function that returns the number best frequency. For
        Synchronous vibration methods, this is the EO.
        
        Parameters
        ----------

        number : non negative integer. The EO with the nth 
            lowest residual.

        Returns
        -------

        The EO with the nth lowest residual.
        '''
        if len(self.EOs) == 0:
            return None
        return self.EOs[ __np__.argsort(self.residuals)[n]]

class uLSSASynchronous(__method__):
    ''' 
    Implements the unimodal Least Squares Spectral Analysis 
    Method for a range of for Synchronous vibration.
    '''
    def __init__(self, offset=False):
        super(__method__, self).__init__()
        self.EOs = []
        self.params = []
        self.residuals = []
        self.offset = offset
    def setData(self, X, AoA):
        ''' 
        Method used to the data of the method. 

        Parameters
        ----------

        X : An NxM Vibrapy signal containing tip deflections from N 
            revolutions and M sensors.

        AoA : An NxM Vibrapy signal containing Angle of Arrivals from
        N revolutions and M sensors.
        '''
        self.X = X.values().flatten()
        self.AoA = AoA.values().flatten()
        self.N = X.shape[0]
        self.M = X.shape[1]
        self.NM = self.N * self.M
    def fit(self, EOs=None, EOmin = None, EOmax = None):
        '''
        A function used to fit the unimodal Least Squares Spectral Analysis method.
        
        Parameters
        ----------

        EOs : A list of EOs for which to fit the LS method. Is not used if EOmin and
            EOmax is specified.

        EOmin : The minimum EO to fit. Is used rather than EOs.

        EOmax : The maximum EO to fit. Is used rather than EOs.
        '''

        if __types__.isNotNone(EOmin) and __types__.isNotNone(EOmax):
            EOs = __np__.arange(EOmin, EOmax+1, 1)
        elif __types__.isNone(EOs):
            raise ValueError('You must specify either EOs or both EOmin and EOmax.')
        self.EOs     = EOs
        self.residuals = []
        self.params = []
        
        cutoff = 2 if self.offset == False else 3
        for EO in self.EOs:
            Phi = __np__.array([__np__.sin(EO*self.AoA),
                                __np__.cos(EO*self.AoA),
                                __np__.ones(len(self.AoA))]).T[:, :cutoff]
            results = __np__.linalg.lstsq(Phi, self.X)
            residual_calc = __np__.sum((Phi.dot(  __np__.array([results[0]]).T).flatten() - self.X)**2)
            self.params.append(1*results[0]) 
            try:
                #self.residuals.append(1*results[1][0])
                self.residuals.append(1*residual_calc)
            except:
                self.residuals.append(-1)
        self.residuals = __np__.array(self.residuals)
        self.residuals[self.residuals==-1] = self.residuals.max()
    def freqSignal(self):
        '''
        A function that returns the residuals as a vibrapy signal.
        '''
        if len(self.EOs) == 0:
            return None
        return __vp__.Signal(-1 * self.residuals, base=self.EOs, signalNames='Residual', signalType='Fit quality', signalUnit='(mu m)^2',base_type='Frequency',base_unit='EO')
    def paramSignal(self):
        '''
        A function that returns the parameters for each EO as a vibrapy signal.
        '''
        if len(self.EOs) == 0:
            return None
        return __vp__.Signal(*__np__.array(self.params), base=self.EOs, signalNames='A', signalType='Coefficients', signalUnit='mu m',base_type='Frequency',base_unit='EO')
    def bestFreq(self, n=0):
        '''
        A function that returns the number best frequency. For
        Synchronous vibration methods, this is the EO.
        
        Parameters
        ----------

        number : non negative integer. The EO with the nth 
            lowest residual.

        Returns
        -------

        The EO with the nth lowest residual.
        '''
        if len(self.EOs) == 0:
            return None
        return self.EOs[ __np__.argsort(self.residuals)[n]]

class NUDFTSynchronous(__method__):
    ''' 
    Implements the Non Uniform Discrete Fourier Transform 
    for a range of for Synchronous vibration.
    '''
    def __init__(self):
        super(__method__, self).__init__()
        self.EOs = []
        self.powers = []
    def setData(self, X, AoA):
        ''' 
        Method used to the data of the method. 

        Parameters
        ----------

        X : An NxM Vibrapy signal containing tip deflections from N 
            revolutions and M sensors.

        AoA : An NxM Vibrapy signal containing Angle of Arrivals from
        N revolutions and M sensors.
        '''
        self.X = X.values().flatten()
        self.AoA = AoA.values().flatten()
        self.N = X.shape[0]
        self.M = X.shape[1]
        self.NM = self.N * self.M
    def fit(self, EOs=None, EOmin = None, EOmax = None):
        '''
        A function used to fit the synchronous Non Uniform Discrete Fourier Transform.
        
        Parameters
        ----------

        EOs : A list of EOs for which to fit the NUDFT method. Is not used if EOmin and
            EOmax is specified.

        EOmin : The minimum EO to fit. Is used rather than EOs. Set to 0 to
            include a DC offset term.

        EOmax : The maximum EO to fit. Is used rather than EOs.
        '''

        if __types__.isNotNone(EOmin) and __types__.isNotNone(EOmax):
            EOs = __np__.arange(EOmin, EOmax+1, 1)
        elif __types__.isNone(EOs):
            raise ValueError('You must specify either EOs or both EOmin and EOmax.')
        self.EOs = EOs
        self.powers  = []
        
        Phi = __np__.array([ __np__.exp(-1j * EO * self.AoA ) for EO in self.EOs]).T
        result =  1 / (self.NM**2) * __np__.absolute(__np__.array([self.X]).dot(Phi))**2
        
        self.powers = 1*result.flatten()
    def freqSignal(self):
        '''
        A function that returns the EO powers as a vibrapy signal.
        '''
        if len(self.EOs) == 0:
            return None
        return __vp__.Signal(self.powers, base=self.EOs, signalNames='Power', signalType='Power Spectral Density', signalUnit='(mu m)^2',base_type='Frequency',base_unit='EO')
    def bestFreq(self, n=0):
        '''
        A function that returns the number best frequency. For
        Synchronous vibration methods, this is the EO.
        
        Parameters
        ----------

        number : non negative integer. The EO with the nth 
            lowest residual.

        Returns
        -------

        The EO with the nth lowest residual.
        '''
        if len(self.EOs) == 0:
            return None
        return self.EOs[ __np__.argsort(self.powers)[::-1][n]]

class uLSSAAsynchronous(__method__):
    ''' 
    Implements the unimodal Least Squares Spectral Analysis method  
    for a range of asynchronous vibration frequencies.
    '''
    def __init__(self, offset=False):
        super(__method__, self).__init__()
        self.freqs = []
        self.params = []
        self.residuals = []
        self.offset = offset
    def setData(self, X, ToA):
        ''' 
        Method used to the data of the method. 

        Parameters
        ----------

        X : An NxM Vibrapy signal containing tip deflections from N 
            revolutions and M sensors.

        AoA : An NxM Vibrapy signal containing Angle of Arrivals from
        N revolutions and M sensors.
        '''
        self.X = X.values().flatten()
        self.ToA = ToA.values().flatten()
        self.N = X.shape[0]
        self.M = X.shape[1]
        self.NM = self.N * self.M
    def fit(self, freqs=None, freq_min = None, freq_max = None, freq_inc=None):
        '''
        A function used to fit the Least Squares Spectral Analysis Method.
        
        Parameters
        ----------

        freqs : A list of frequencies, in Hz, for which to fit the NUFDT method. Is not
            used if freq_min, freq_max and freq_inc is specified.

        freq_min : The minimum freq, in Hz, to fit. Is used rather than freqs. Set to 0 to
            include a DC offset term.

        freq_max : The maximum frequency, in Hz, to fit. Is used rather than EOs.

        freq_inc : The frequency step to use for calculating the NUDFT.
        '''
        if __types__.isNotNone(freq_min) and __types__.isNotNone(freq_max) and __types__.isNotNone(freq_inc):
            freqs = __np__.arange(freq_min, freq_max+freq_inc, freq_inc)
        elif __types__.isNone(freqs):
            raise ValueError('You must specify either freqs or all of freq_min, freq_max and freq_inc')
        self.freqs = freqs
        
        self.params  = []
        self.residuals = []
        cutoff = 2 if self.offset == False else 3
        for freq in self.freqs:
            Phi = __np__.array([ __np__.sin(2 * __np__.pi * freq * self.ToA),
                                 __np__.cos(2 * __np__.pi * freq * self.ToA),
                                 __np__.ones_like(self.ToA)]).T[:, :cutoff]

            result = __np__.linalg.lstsq(Phi, self.X)
            
            self.params.append(1*result[0])
            if freq != 0:
                self.residuals.append(1*result[1][0])
            else:
                self.residuals.append(-1)
        self.params = __np__.array(self.params)
        self.residuals = __np__.array(self.residuals)
        self.residuals[self.residuals == -1] = self.residuals.max()
    def freqSignal(self):
        '''
        A function that returns the frequency powers as a vibrapy signal.
        '''
        if len(self.freqs) == 0:
            return None
        return __vp__.Signal(-1 * self.residuals, base=self.freqs, signalNames='Residuals', signalType='Fit quality', signalUnit='(mu m)^2',base_type='Frequency',base_unit='Hz')
    def bestFreq(self, n=0):
        '''
        A function that returns the number best frequency. For
        Synchronous vibration methods, this is the EO.
        
        Parameters
        ----------

        number : non negative integer. The EO with the nth 
            lowest residual.

        Returns
        -------

        The EO with the nth lowest residual.
        '''
        if len(self.freqs) == 0:
            return None
        return self.freqs[ __np__.argsort(self.residuals)[n]]

class NUDFTAsynchronous(__method__):
    ''' 
    Implements the Non Uniform Discrete Fourier Transform for 
    for a range of asynchronous vibration frequencies.
    '''
    def __init__(self):
        super(__method__, self).__init__()
        self.freqs = []
        self.powers = []
    def setData(self, X, ToA):
        ''' 
        Method used to the data of the method. 

        Parameters
        ----------

        X : An NxM Vibrapy signal containing tip deflections from N 
            revolutions and M sensors.

        AoA : An NxM Vibrapy signal containing Angle of Arrivals from
        N revolutions and M sensors.
        '''
        self.X = X.values().flatten()
        self.ToA = ToA.values().flatten()
        self.N = X.shape[0]
        self.M = X.shape[1]
        self.NM = self.N * self.M
    def fit(self, freqs=None, freq_min = None, freq_max = None, freq_inc=None):
        '''
        A function used to fit the asynchronous Non Uniform Discrete Fourier Transform.
        
        Parameters
        ----------

        freqs : A list of frequencies, in Hz, for which to fit the NUFDT method. Is not
            used if freq_min, freq_max and freq_inc is specified.

        freq_min : The minimum freq, in Hz, to fit. Is used rather than freqs. Set to 0 to
            include a DC offset term.

        freq_max : The maximum frequency, in Hz, to fit. Is used rather than EOs.

        freq_inc : The frequency step to use for calculating the NUDFT.
        '''
        if __types__.isNotNone(freq_min) and __types__.isNotNone(freq_max) and __types__.isNotNone(freq_inc):
            freqs = __np__.arange(freq_min, freq_max+freq_inc, freq_inc)
        elif __types__.isNone(freqs):
            raise ValueError('You must specify either freqs or all of freq_min, freq_max and freq_inc')
        self.freqs = freqs
        self.powers  = []
        

        Phi = __np__.array([ __np__.exp(-1j * 2 * __np__.pi * freq * self.ToA) for freq in self.freqs]).T

        result =  1 / (self.NM**2) * __np__.absolute(__np__.array([self.X]).dot(Phi))**2
        
        self.powers = 1*result.flatten()
    def freqSignal(self):
        '''
        A function that returns the frequency powers as a vibrapy signal.
        '''
        if len(self.freqs) == 0:
            return None
        return __vp__.Signal(self.powers, base=self.freqs, signalNames='Power', signalType='Power Spectral Density', signalUnit='(mu m)^2',base_type='Frequency',base_unit='Hz')
    def bestFreq(self, n=0):
        '''
        A function that returns the number best frequency. For
        Synchronous vibration methods, this is the EO.
        
        Parameters
        ----------

        number : non negative integer. The EO with the nth 
            lowest residual.

        Returns
        -------

        The EO with the nth lowest residual.
        '''
        if len(self.freqs) == 0:
            return None
        return self.freqs[ __np__.argsort(self.powers)[::-1][n]]

class ensembleSynchronous(__method__):
    ''' 
    An ensemble method for estimating the dominant EOs in a BTT signal.
    
    Parameters
    ----------

    estimator : An base estimator object. The object must possess the
        setData, fit and bestFreq functions as in uLSSASynchronous. 
        Defaults to uLSSASynchronous if not specified.

    n_min : The minimum number of revolutions to use for estimation. 
    
    rule : The rule to use for picking possible EOs. Defaults to
        the 'Winner Takes All' method.

    winners : The number of winners to pick for each analysis. Is only used
        when rule == 'MW' and defaults to 3.
    '''
    def __init__(self, estimator=None, m_min=1, rule='WTA', winners=3, normalize=True):
        super().__init__()
        if __types__.isNone(estimator):
            self.estimator = uLSSASynchronous()
        else:
            self.estimator = estimator
        if __types__.isNatural(m_min):
            self.m_min = m_min
        else:
            __types__.isNatural(m_min, 'm_min')
        possible_rules = ['WTA', 'MW']
        if rule in ['WTA', 'MW']:
            self.rule = str(rule)
        else:
            raise ValueError('The rule must be in the list: {}'.format(possible_rules))
        if __types__.isNatural(winners):
            self.winners = winners
        else:
            __types__.isNatural(winners, 'winners')
        self.EO_estimates = []
        self.normalize = normalize
    def setData(self, X, AoA):
        '''
        Method used to the data of the method. 

        Parameters
        ----------

        X : An NxM Vibrapy signal containing tip deflections from M 
            revolutions and N sensors.

        AoA : An NxM Vibrapy signal containing Angle of Arrivals from
        M revolutions and N sensors.
        '''
        self.X = X[:,:]
        self.AoA = AoA[:,:]
        self.M = X.shape[0]
        self.N = X.shape[1]
    def fit(self, EOs=None, EOmin = None, EOmax = None):
        if __types__.isNotNone(EOmin) and __types__.isNotNone(EOmax):
            EOs = __np__.arange(EOmin, EOmax+1, 1)
        elif __types__.isNone(EOs):
            raise ValueError('You must specify either EOs or both EOmin and EOmax.')
        self.EOs     = EOs

        EO_estimates = __np__.zeros_like(EOs)
        for scope in range(self.m_min, self.M+1):
            begin_index = 0
            while begin_index < self.M:
                self.estimator.setData(self.X[begin_index:begin_index+scope, :],self.AoA[begin_index:begin_index+scope, :])
                self.estimator.fit(EOs=self.EOs)
                if self.rule =='WTA':
                    EO_estimates[__np__.where(self.EOs == self.estimator.bestFreq(0))[0][0]] += 1
                elif self.rule =='MW':
                    for i in range(self.winners):
                        EO_estimates[__np__.where(self.EOs == self.estimator.bestFreq(i))[0][0]] += 1 * (self.winners - i)
                begin_index += scope
        self.EO_estimates = EO_estimates
        if self.normalize: self.EO_estimates =  self.EO_estimates / __np__.sum(self.EO_estimates)
    def freqSignal(self):
        if len(self.EO_estimates) == 0:
            return None
        return __vp__.Signal(self.EO_estimates, base=self.EOs, signalNames='Occurrences', signalType='Probability', signalUnit='.',base_type='Frequency',base_unit='EO')
    def bestFreq(self, n=0):
        '''
        A function that returns the number best frequency. For
        Synchronous vibration methods, this is the EO.
        
        Parameters
        ----------

        number : non negative integer. The EO with the nth 
            lowest residual.

        Returns
        -------

        The EO with the nth lowest residual.
        '''
        if len(self.EO_estimates) == 0:
            return None
        return self.EOs[ __np__.argsort(self.EO_estimates)[::-1][n]]

class ensembleAsynchronous(__method__):
    ''' 
    An ensemble method for estimating the dominant frequencies in a BTT signal.
    
    Parameters
    ----------

    estimator : An base estimator object. The object must possess the
        setData, fit and bestFreq functions as in uLSSAAsynchronous. 
        Defaults to uLSSASynchronous if not specified.

    m_min : The minimum number of revolutions to use for estimation. 
    
    rule : The rule to use for picking possible EOs. Defaults to
        the 'Winner Takes All' method.

    winners : The number of winners to pick for each analysis. Is only used
        when rule == 'MW' and defaults to 3.
    '''
    def __init__(self, estimator=None, m_min=1, rule='WTA', winners=3, normalize=True):
        super().__init__()
        if __types__.isNone(estimator):
            self.estimator = uLSSAAsynchronous()
        else:
            self.estimator = estimator
        if __types__.isNatural(m_min):
            self.m_min = m_min
        else:
            __types__.isNatural(m_min, 'm_min')
        possible_rules = ['WTA', 'MW']
        if rule in ['WTA', 'MW']:
            self.rule = str(rule)
        else:
            raise ValueError('The rule must be in the list: {}'.format(possible_rules))
        if __types__.isNatural(winners):
            self.winners = winners
        else:
            __types__.isNatural(winners, 'winners')
        self.freq_estimates = []
        self.normalize = normalize
    def setData(self, X, ToA):
        ''' 
        Method used to the data of the method. 

        Parameters
        ----------

        X : An NxM Vibrapy signal containing tip deflections from M 
            revolutions and N sensors.

        AoA : An NxM Vibrapy signal containing Angle of Arrivals from
        M revolutions and N sensors.
        '''
        self.X = X[:,:]
        self.ToA = ToA[:,:]
        self.M = X.shape[0]
        self.N = X.shape[1]
    def fit(self, freqs=None, freq_min = None, freq_max = None, freq_inc=None):
        '''
        A function used to fit the asynchronous ensemble method to Fourier Transform.
        
        Parameters
        ----------

        freqs : A list of frequencies, in Hz, for which to fit the NUFDT method. Is not
            used if freq_min, freq_max and freq_inc is specified.

        freq_min : The minimum freq, in Hz, to fit. Is used rather than freqs. Set to 0 to
            include a DC offset term.

        freq_max : The maximum frequency, in Hz, to fit. Is used rather than EOs.

        freq_inc : The frequency step to use for calculating the ensemble frequencies.
        '''

        if __types__.isNotNone(freq_min) and __types__.isNotNone(freq_max) and __types__.isNotNone(freq_inc):
            freqs = __np__.arange(freq_min, freq_max+freq_inc, freq_inc)
        elif __types__.isNone(freqs):
            raise ValueError('You must specify either freqs or all of freq_min, freq_max and freq_inc')
        self.freqs = freqs

        freq_estimates = __np__.zeros_like(freqs)
        for scope in range(self.m_min, self.M+1):
            begin_index = 0
            while begin_index < self.M:
                self.estimator.setData(self.X[begin_index:begin_index+scope, :],self.ToA[begin_index:begin_index+scope, :])
                self.estimator.fit(freqs=self.freqs)
                if self.rule =='WTA':
                    freq_estimates[__np__.where(self.freqs == self.estimator.bestFreq(0))[0][0]] += 1
                elif self.rule =='MW':
                    for i in range(self.winners):
                        freq_estimates[__np__.where(self.freqs == self.estimator.bestFreq(i))[0][0]] += 1 * (self.winners - i)
                begin_index += scope
        self.freq_estimates = freq_estimates
        if self.normalize: self.freq_estimates = self.freq_estimates / __np__.sum(self.freq_estimates) 
    def freqSignal(self):
        if len(self.freq_estimates) == 0:
            return None
        return __vp__.Signal(self.freq_estimates, base=self.freqs, signalNames='Occurrences', signalType='Probability', signalUnit='.',base_type='Frequency',base_unit='Hz')
    def bestFreq(self, n=0):
        '''
        A function that returns the number best frequency. For
        Synchronous vibration methods, this is the EO.
        
        Parameters
        ----------

        number : non negative integer. The EO with the nth 
            lowest residual.

        Returns
        -------

        The EO with the nth lowest residual.
        '''
        if len(self.freq_estimates) == 0:
            return None
        return self.freqs[ __np__.argsort(self.freq_estimates)[::-1][n]]