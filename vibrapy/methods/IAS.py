'''
Module for containing Methods to work with Instantaneous Angular Speed (IAS) from zero
crossing times and the encoder geometry. 
===============================================
Authors: David Hercules Diamond

License: MIT
'''

import warnings

from numpy import array, identity, ediff1d, sqrt, pi, insert,min, max, sign, diff, arange, insert
from numpy.linalg import inv
from matplotlib import pyplot as plt


import numpy as np
from scipy.sparse.linalg import spsolve
from scipy.sparse import csr_matrix
from vibrapy.utils import types

def extractZeroCrossingThreshold(signal, threshhold=None, lower_lim=None, higher_lim=None, gradient_positive=True, min_sep=5, dt=None, disp=True):
    """
    Function to extract the zero-crossing times from a signal using a thresholding technique with
    automatic interpolation.

    Parameters
    ----------

    signal   : NDarray, list or Pandas series containing the signal values at each time increment. 

    threshold : float or string. Variable indicating the threshold value used to determine the zero-crossing time.  Threshold value is taken directly for a float value and is calculated as a percentage of the range if a string is given.  String is converted to an float between 0 and 1.  If the value is out of this range the threshold is set to 0.5 times the range.


    lower_lim : float.  Value indicating the lower limit of the signal.  Is calculated if not
            given.

    higher_lim : float.  Value indicating the higher limit of the signal. Is calculated if not
            given. 

    gradient_positive : bool.  Indicated whether the signal should be triggered on the rise or fall of the signal.  Defaults to True.

    min_sep : int.  Minimum seperation between consequtive zero-crossing times.  Defaults to  5.

    dt : float.  Time between consecutive samples.  Defaults to 1.
    
    disp: boolean.  Indicates whether the thresholding parameters should be displayed.

    Returns
    -------
    zero_crossings : NDArray.  Array containing the zero-crossing times.
    """

    # Determine min and max if not given
    if not lower_lim:
        lower_lim = min(signal)
    if not higher_lim:
        higher_lim = max(signal)
    if types.isNum(lower_lim) == False:
        raise ValueError('Lower limit of {} is not a valid number.'.format(
            lower_lim))
    if types.isNum(higher_lim) == False:
        raise ValueError('Higher limit of {} is not a valid number.'.format(
            higher_lim))
    if dt is None:
        warnings.warn('The time increment has not been specified.  Defaulting to dt=1')
        dt = 1
    elif types.isNum(dt) == False:
        raise ValueError('dt value of {} is not a valid number.'.format(
            dt))
    # Determine signal range
    signal_range = higher_lim - lower_lim
    # Determine threshold value if not given as in the middle of the range
    if not threshhold:
        threshhold = '0.5'
    # Determine the threshold value from the argument
    if threshhold is None:
        threshhold = '0.5'
    if isinstance(threshhold, str):
        try:
            threshhold = float(threshhold)
            if not (threshhold <=1 and threshhold >=0):
                warnings.warn('The threshold value is not within the range 0 - 1.  Defaulting to 0.5.')
                threshhold = 0.5
        except:
            raise ValueError("Cannot convert the threshold value {} to a float between 0 and 1.  Please pass a convertable string".format(threshhold))
        threshhold = lower_lim + signal_range*threshhold
    elif types.isNum(threshhold):
        threshhold = threshhold
    else:
        raise ValueError("Cannot interpret the threshold value {} as a valid threshold.".format(threshhold))
    if threshhold < lower_lim or threshhold > higher_lim:
        raise ValueError("The threshhold value of {} is smaller than the lower limit of {} or higher than the higher limit of {}.".format(threshhold, lower_lim, higher_lim))
    if not isinstance(gradient_positive, bool):
        raise ValueError("gradient_positive value of {} is not a valid boolean value.".format(threshhold))
    if gradient_positive:
        gradient_value = 2
    else:
        gradient_value = -2
    if disp:
        print('Extract parameters ----------')
        print('Lower limit:', lower_lim)
        print('Higher limit:', higher_lim)
        print('Signal Range:', signal_range)
        print('Threshold:', threshhold)
        print('-----------------------------')
    # Determine the indices of the first values after the zero crossing
    signed_signal_diff = diff(sign(array(signal) - threshhold))
    indices_diff_cross = arange(1, len(signal))[signed_signal_diff == gradient_value]
    # Perform interpolation
    zero_crossings = indices_diff_cross -\
                     (signal[indices_diff_cross] - threshhold )\
                    /(signal[indices_diff_cross] - signal[indices_diff_cross-1])
    # Remove zero-crossings that are closer than min_sep to the previous one
    no = 0
    while True:
        no += 1
        if no == 10:
            warnings.warn('This is the tenth time that double zero-crossings are removed.  This indicates that a threshhold value has not been set properly.  Please check that the identified zero-crossing times are correct and as I am now stopping the removal of double triggers.')
            break
        space              = insert(ediff1d(zero_crossings), 0, min_sep + 1)
        signed_space_diff  = diff(sign(space - min_sep))
        if sum(signed_space_diff == 0) == len(signed_space_diff):
            break
        keep_list          = insert(1 + arange(len(signed_space_diff))[signed_space_diff != -2], 0, 0).astype(int)
        zero_crossings = zero_crossings[keep_list]
    # Operation complete, return zero-crossing times
    return zero_crossings * dt

def performBayesianGeometryCompensation(t,N,M,e=[],beta=10.0e10,sigma=10.0):
    '''
    PerformBayesianGeometryCompensation(t,N,M,e=[],beta=10.0e10,sigma=10.0)

    Perform geometry compensation on an incremental shaft encoder with N sections measured over M revolutions.

    Parameters
    ----------
    t :     1D numpy array of zeros crossing times.  The first zero crossing time indicates
        the start of the first section.  This array should therefore have exactly M*N + 1 elements.
    N:  The number of sections in the shaft encoder.
    M:  The number of complete revolutions over which the compensation must be performed.
    e:  An initial estimate for the encoder geometry.  If left an empty array, all sections are assumed equal.
    beta:   Precision of the likelihood function.
    sigma:  Standard deviation of the prior probability.     
    
    Returns
    -------
    epost : An array containing the circumferential distances of all N sections.
    '''
    if len(t) != M*N + 1:
        print('Input Error: The vector containing the zero-crossing times should contain exactly N*M + 1 values')
        raise SystemExit
    if len(e) != 0 and len(e) != N:
        print('Input Error The encoder input should either be an empty list or a list with N elements')
        raise SystemExit
    # Initialize matrices
    A = np.zeros((2*M*N - 1,N + 2*M*N))
    B = np.zeros((2*M*N - 1,1))
    # Calculate zero-crossing periods
    T = np.ediff1d(t)
    # Insert Equation (11)
    A[0,:N] = np.ones(N)
    B[0,0] = 2*np.pi
    # Insert Equation (9) into A
    deduct = 0
    for m in range(M):
        if m==M-1:
            deduct = 1
        for n in range(N-deduct):
            nm = m*N + n
            A[1 + nm,n] =                 3.0
            A[1 + nm,N + nm*2] =         -1.0/2 * T[nm]**2
            A[1 + nm,N + nm*2 + 1] =     -2 * T[nm]
            A[1 + nm,N + (nm+1)*2 + 1] = -1 * T[nm]
    # Insert Equation (10) into A
    deduct = 0
    for m in range(M):
        if m==M-1:
            deduct = 1
        for n in range(N-deduct):
            nm = m*N + n
            A[M*N + nm,n] =                 6.0
            A[M*N + nm,N + nm*2] =         -2 * T[nm]**2
            A[M*N + nm,N + (nm+1)*2] =     -1 * T[nm]**2
            A[M*N + nm,N + nm*2 + 1] =     -6 * T[nm]
    # Initialize prior vector
    m0 = np.zeros((N + 2*M*N,1))
    # Initialize and populate covariance matrix of prior
    Sigma0 = np.identity(N + 2*M*N) * sigma**2
    # Populate prior vector
    if len(e) == 0:
        eprior = np.ones(N) * 2*np.pi/N
    else:
        eprior = np.array(e) * 1.0
    m0[:N,0] = eprior * 1.0
    for m in range(M):
        for n in range(N):
            nm = m*N + n
            m0[N + nm*2 + 1,0] = m0[n,0] / T[nm]
    # Solve for mN (or x)
    SigmaN = Sigma0 + beta * A.T.dot(A)
    BBayes = Sigma0.dot(m0) + beta * A.T.dot(B)

    mN = np.array([spsolve(csr_matrix(SigmaN),csr_matrix(BBayes))]).T
    
    # Normalize encoder increments to add up to 2 pi
    epost = mN[:N,0] *2*np.pi / (np.sum(mN[:N,0]))
    # Return encoder geometry
    return epost

def IAS_MPR(MPR, M, geom, fs):
    def kryMats(m,T,mu_min1,V_min1):
        C = array([[0.5*T[m],0.5*T[m]]])
        if m > 0:
            #Vir alle toestande na 1
            A = array([[0,1], [-T[m]/T[m-1],1 + T[m]/T[m-1]]])
            Gamma = (   0.5 *  0.01 * A.dot(mu_min1) * identity(2)    )  **2
            P_min1 = A.dot(V_min1).dot(A.T) + Gamma
            Sigma = array((0.25 * 1/fs * mu_min1.T.dot(A.T.dot(array([[1,1]]).T))))**2
            K = P_min1.dot(C.T) * inv((C.dot(P_min1.dot(C.T)) + Sigma))
        else:
            #Vir eerste toestand
            A = array([[1,0], [0,1]])
            Gamma = (0.5 * 0.01 * A.dot(mu_min1) * identity(2))**2
            P_min1 = A.dot(V_min1).dot(A.T) + Gamma
            Sigma = array((0.25 * 1/fs * mu_min1.T.dot(A.T.dot(array([[1,1]]).T))))**2
            K = P_min1.dot(C.T) * inv((C.dot(P_min1.dot(C.T)) + Sigma))
        return A,C,P_min1,Gamma,Sigma,K
    fs = fs
    S = len(geom)
    T = ediff1d(MPR)
    
    mu=[array([[geom[0]/T[0], geom[1]/T[1] ]]).T]
    V =[ (0.005 * mu[0]* identity(2))**2]
    x = []
    for s in range(S):
        x.append(array([[geom[s]]]))
    for m in range(M):
        for s in range(S):
            A,C,P_min1,Gamma,Sigma,K = kryMats(m*S+s,T,mu[-1],V[-1])
            mu.append(A.dot(mu[-1]) + K.dot( x[s] - C.dot(A.dot(mu[-1]))))
            V.append((identity(2) - K.dot(C)).dot(P_min1))
    mu,V = array(mu)[1:,:,:], array(V)[1:,:,:]
    
    a_mu =  1/T*mu[:,1,0] - 1/T * mu[:,0,0]
    a_var = (1/T)**2 * V[:,1,1] - (1/T)**2 * V[:,0,0] - 2/T**2 * V[:,0,1]
    return insert(mu[:,0,0], len(mu[:,0,0]), mu[-1,1,0]),\
           insert(V[:,0,0], len(V[:,0,0]), V[-1,1,1] ), a_mu, a_var

def IAS_OPR(OPR, geom, fs):
    def kryMats(m,T,mu_min1,V_min1):
        C = array([[0.5*T[m],0.5*T[m]]])
        if m > 0:
            #Vir alle toestande na 1
            A = array([[0,1], [-T[m]/T[m-1],1 + T[m]/T[m-1]]])
            Gamma = (   0.5 *  0.01 * A.dot(mu_min1) * identity(2)    )  **2
            P_min1 = A.dot(V_min1).dot(A.T) + Gamma
            Sigma = array((0.25 * 1/fs * mu_min1.T.dot(A.T.dot(array([[1,1]]).T))))**2
            K = P_min1.dot(C.T) * inv((C.dot(P_min1.dot(C.T)) + Sigma))
        else:
            #Vir eerste toestand
            A = array([[1,0], [0,1]])
            Gamma = (0.5 * 0.01 * A.dot(mu_min1) * identity(2))**2
            P_min1 = A.dot(V_min1).dot(A.T) + Gamma
            Sigma = array((0.25 * 1/fs * mu_min1.T.dot(A.T.dot(array([[1,1]]).T))))**2
            K = P_min1.dot(C.T) * inv((C.dot(P_min1.dot(C.T)) + Sigma))
        return A,C,P_min1,Gamma,Sigma,K
    fs = fs
    T = ediff1d(OPR)
    M = len(T)
    mu=[array([[2*pi/T[0], 2*pi/T[1] ]]).T]
    V=[ (0.005 * mu[0]* identity(2))**2]
    x = array([[2*pi]])
    for m in range(M):
        A,C,P_min1,Gamma,Sigma,K = kryMats(m,T,mu[-1],V[-1])
        mu.append(A.dot(mu[-1]) + K.dot( x - C.dot(A.dot(mu[-1])))  )
        V.append((identity(2) - K.dot(C)).dot(P_min1) )
    mu,V = array(mu)[1:,:,:], array(V)[1:,:,:]
    a_mu = 1/T*mu[:,1,0] - 1/T * mu[:,0,0]
    a_var = (1/T)**2 * V[:,1,1] - (1/T)**2 * V[:,0,0] - 2/T**2 * V[:,0,1]
    return insert(mu[:,0,0], len(mu[:,0,0]), mu[-1,1,0]),\
           insert(V[:,0,0], len(V[:,0,0]), V[-1,1,1] ), a_mu, a_var
