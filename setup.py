from setuptools import setup

setup(name='vibrapy',
      packages=['vibrapy'],
      include_package_data=True,
      install_requires=['pandas',
                        'matplotlib',
                        'numpy',
                        ])
